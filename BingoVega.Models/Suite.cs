﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BingoVega.Models
{
    public class Suite
    {
        public SuiteAttributes _attributes { get; set; }

        public GameA Games { get; set; }
    }

    public class SuiteAttributes
    {

        public string ID { get; set; }
        public string Name { get; set; }
    }
}
