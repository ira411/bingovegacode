﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BingoVega.Models
{
    public class CreditCard
    {
        public string UserId { get; set; }
        public int Id { get; set; }
        public string CardTypeDescription { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public int ExpiryYear { get; set; }
        public int ExpiryMonth { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string Postalcode { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }

    }
}
