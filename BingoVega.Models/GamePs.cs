﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BingoVega.Models
{
    public class GamePs
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Logo { get; set; }
        public string Langs { get; set; }
        public int Order { get; set; }
        public string DisplayType { get; set; }
    }
}
