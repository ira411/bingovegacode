﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BingoVega.Models
{
    public class GameA
    {
        public List<GameObject> Game { get; set; }
    }


    public class GameObject { 
    
        public GameAttributes _attributes { get; set; }
    }

    public class GameAttributes
    {

        public string ID { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }

        public string Languages { get; set; }
        public int Order { get; set; }
        public string DisplayType { get; set; }
    }
}
