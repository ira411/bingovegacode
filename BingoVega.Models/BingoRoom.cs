﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BingoVega.Models
{
    public class BigoRoom
    {

        public int RoomId { get; set; }
        public string ChatRoomId { get; set; }
        public int ScheduleId { get; set; }

        public string GameType { get; set; }
        public string RoomName { get; set; }
        public int DisplayOrder { get; set; }
        public string Skin { get; set; }
        public string Caller { get; set; }
        public string Announcer { get; set; }
        public string ChatRoom { get; set; }
        public string Status { get; set; }
        public string ScheduleName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
