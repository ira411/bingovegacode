﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BingoVega.Models
{
    public class SpecialSuite
    {
        public SuiteAttributes _attributes { get; set; }

        public SpecialGame Games { get; set; }
    }
}
