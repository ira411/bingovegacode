﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.Models
{
    public class CreditCardEncripted
    {
        public int Id { get; set; }
        public string CardTypeDescription { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string ExpiryYear { get; set; }
        public string ExpiryMonth { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string Postalcode { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }


    }
}
