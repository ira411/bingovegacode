﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.Services
{
    public interface IDapper : IDisposable
    {

        DbConnection GetDbconnection(string Connectionstring = "DefaultConnection");
        T Get<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure, string Connectionstring = "DefaultConnection");
        List<T> GetAll<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure, string Connectionstring = "DefaultConnection");
        int Execute(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure, string Connectionstring = "DefaultConnection");
        T Insert<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure, string Connectionstring = "DefaultConnection");
        T Update<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure, string Connectionstring = "DefaultConnection");
    }
}
