﻿using BingoVega.API.ViewModels;
using BingoVega.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BingoVega.API.Controllers
{
    [Route("api/games")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private IConfiguration _config;


        public GamesController(IConfiguration config)
        {

            _config = config;
        }

        [HttpGet]
        [Route("getgames")]
        public IActionResult GetGames() {


            //BingoRoomViewModel bingoRoom = new BingoRoomViewModel();


            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string url = "http://172.22.2.87:3001/api/slot/fetchgamelist";
            var c = new StringContent("", Encoding.UTF8, "application/json");
            var result = client.PostAsync(url,c).Result;
            List<Suite> suiteList = new List<Suite>();
            GamesViewModel viewModel = new GamesViewModel();
            if (result.IsSuccessStatusCode)
            {
                var responseString = result.Content.ReadAsStringAsync().Result;
                var feed = JObject.Parse(responseString);//JsonConvert.DeserializeObject<BingoRoomViewModel>(responseString);

                var games = feed.SelectToken("data.slot.engines.NG.games.GAMESSUITES.SUITES.SUITE").ToList();
                foreach (var token in games)
                {

                    try
                    {
                        var suite = token.ToObject<Suite>();
                        suiteList.Add(suite);
                    }
                    catch (Exception ex)
                    {

                        var specialSuite = token.ToObject<SpecialSuite>();
                        Suite suite = new Suite();
                        suite._attributes = specialSuite._attributes;
                        suite.Games = new GameA();
                        suite.Games.Game = new List<GameObject>();
                        GameAttributes gameAtt = new GameAttributes();
                        gameAtt.DisplayType = specialSuite.Games.Game._attributes.DisplayType;
                        gameAtt.ID = specialSuite.Games.Game._attributes.ID;
                        gameAtt.ImageUrl = specialSuite.Games.Game._attributes.ImageUrl;
                        gameAtt.Languages = specialSuite.Games.Game._attributes.Languages;
                        gameAtt.Name = specialSuite.Games.Game._attributes.Name;
                        gameAtt.Order = specialSuite.Games.Game._attributes.Order;
                        GameObject gameObj = new GameObject();
                        gameObj._attributes = gameAtt;
                        suite.Games.Game.Add(gameObj);
                        suiteList.Add(suite);
                    }

                }
                List<GamePs> gameList = new List<GamePs>();
                var gamesPs = feed.SelectToken("data.slot.engines.PS.games").ToList();
                foreach (var token in gamesPs)
                {
                    var game = token.ToObject<GamePs>();
                    gameList.Add(game);
                }
               
                viewModel.NgGames = suiteList;
                viewModel.PsGames = gameList;
            }

            return Ok(viewModel);
        }
    }
}
