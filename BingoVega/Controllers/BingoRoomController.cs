﻿using BingoVega.API.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BingoVega.API.Controllers
{
    [Route("api/bingorooms")]
    [ApiController]
    public class BingoRoomController : ControllerBase
    {
        private IConfiguration _config;


        public BingoRoomController(IConfiguration config)
        {

            _config = config;
        }

        [HttpGet]
        [Route("getrooms")]
        public IActionResult GetBingoRooms()
        {

            BingoRoomViewModel bingoRoom = new BingoRoomViewModel();


            HttpClient client = new HttpClient();
            string url = "http://172.22.2.87:3001/api/bingo/fetch/lobbyrooms";
            var result = client.GetAsync(url).Result;
            if (result.IsSuccessStatusCode)
            {
                var responseString = result.Content.ReadAsStringAsync().Result;
                bingoRoom = JsonConvert.DeserializeObject<BingoRoomViewModel>(responseString);

            }

            return Ok(bingoRoom.Data);
        }
    }
}
