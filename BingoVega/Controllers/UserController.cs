﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using BingoVega.Models;
using BingoVega.Services;
using BingoVega.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.AspNetCore.Cors;
using BingoVega.Security;

namespace BingoVega.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IDapper _dapper;
        private IConfiguration _config;
        private const string _tripleDesKey = "58d8b19df78558d2d0e5f509";

        private readonly static Dictionary<char, byte> hexmap = new Dictionary<char, byte>()
    {
        { 'a', 0xA },{ 'b', 0xB },{ 'c', 0xC },{ 'd', 0xD },
        { 'e', 0xE },{ 'f', 0xF },{ 'A', 0xA },{ 'B', 0xB },
        { 'C', 0xC },{ 'D', 0xD },{ 'E', 0xE },{ 'F', 0xF },
        { '0', 0x0 },{ '1', 0x1 },{ '2', 0x2 },{ '3', 0x3 },
        { '4', 0x4 },{ '5', 0x5 },{ '6', 0x6 },{ '7', 0x7 },
        { '8', 0x8 },{ '9', 0x9 }
        };

        public UserController(IDapper dapper, IConfiguration config)
        {
            _dapper = dapper;
            _config = config;
        }
        
        [HttpGet("authenticate")]
        public ActionResult Authenticate(string username, string password) {
            UserProfileViewModel userViewModeel = new UserProfileViewModel();
            try
            {               
                var dbparams = new DynamicParameters();
                dbparams.Add("Username", username, DbType.String);
                var user = _dapper.Get<UserProfile>("[dbo].[ACCT_GetUserProfileByUsername]", dbparams);

                if (user == null)
                {
                    return Unauthorized("User not found");
                }                

                string pass = Decryptor.Decrypt(user.Password, _tripleDesKey);

                if (pass == password)
                {                    
                    return Ok();
                }
                else {

                    return Unauthorized("Wrong Password");
                }
            }
            catch (Exception ex) {
                return  StatusCode(StatusCodes.Status500InternalServerError, "An error has occurred, please try again!");
            }

        }

        [HttpGet("getLobbyAccessLevel")]        
        public ActionResult GetLobbyAccessLevel(string userId)
        {
            UserProfileViewModel userViewModeel = new UserProfileViewModel();
            try
            {

                var dbparams = new DynamicParameters();
                dbparams.Add("@accountid", userId, DbType.String);
                bool hasFullAccess = _dapper.Get<bool>("[dbo].[fullLobbyAccess]", dbparams);
                return Ok(hasFullAccess);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }


        [HttpGet("getUser")]
        public ActionResult GetUserProfileByUserName(string username)
        {
            UserProfileViewModel userViewModeel = new UserProfileViewModel();
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("Username", username, DbType.String);
                var user = _dapper.Get<UserProfile>("[dbo].[ACCT_GetUserProfileByUsername]", dbparams);               

                JwtService jwt = new JwtService(_config);

                userViewModeel.UserId = user.UserId;
                userViewModeel.Username = user.Username;
                userViewModeel.FirstName = user.FirstName;
                userViewModeel.LastName = user.LastName;
                userViewModeel.Address = Decryptor.Decrypt(user.Address, _tripleDesKey);
                userViewModeel.City = Decryptor.Decrypt(user.City, _tripleDesKey);
                userViewModeel.Province = Decryptor.Decrypt(user.Province, _tripleDesKey);

                userViewModeel.PostalCode = Decryptor.Decrypt(user.PostalCode, _tripleDesKey);
                userViewModeel.Country = Decryptor.Decrypt(user.Country, _tripleDesKey);
                userViewModeel.Phone = Decryptor.Decrypt(user.Phone, _tripleDesKey);
                userViewModeel.Email = Decryptor.Decrypt(user.Email, _tripleDesKey);
                userViewModeel.Password = Decryptor.Decrypt(user.Password, _tripleDesKey);
              
                userViewModeel.BirthDay = user.BirthDay;
                userViewModeel.Gender = user.Gender;

                userViewModeel.SourceId = user.SourceId;
                userViewModeel.ReceiveBroadCast = user.ReceiveBroadCast;

                userViewModeel.Balance = user.Balance;
                userViewModeel.Winnings = user.Winnings;
                userViewModeel.Bank = user.Bank;
                userViewModeel.Cash = user.Cash;
                userViewModeel.Bonus = user.Bonus;
                userViewModeel.HasFullLobbyAccess = user.HasFullLobbyAccess;
                userViewModeel.CreditCardsAllowed = user.CreditCardsAllowed;
                userViewModeel.MaxBalanceToDeposit = user.MaxBalanceToDeposit;

                userViewModeel.Token = jwt.GenerateSecurityToken(user.Email);
                return Ok(userViewModeel);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
            
        }

        [HttpGet("getCountries")]
        public ActionResult GetCountries()
        {           
            try
            {               
                var countries = _dapper.GetAll<string>("[dbo].[getCountries]", null);
                               
                return Ok(countries);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }


        [HttpPost]
        [Route("addUser")]
        public IActionResult AddUser(UserProfile user)
        {
            try
            {

                var dbparamsSearch = new DynamicParameters();
                dbparamsSearch.Add("Username", user.Username, DbType.String);
                var userDB = _dapper.Get<UserProfile>("[dbo].[ACCT_GetUserProfileByUsername]", dbparamsSearch);

                if (userDB == null)
                {
                    var dbparams = getEncriptedParametersForUser(user);
                    var result = _dapper.Execute("[dbo].[ACCT_AddUserProfile]", dbparams, CommandType.StoredProcedure);
                    string userId = dbparams.Get<String>("UserId");

                    return Ok(userId);
                }
                else
                {

                    return StatusCode(StatusCodes.Status400BadRequest, "Username is already in use");
                }
            }
            catch (Exception ex) {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost]
        [Route("updateUser")]
        public IActionResult UpdateUser(UserProfile user)
        {
            try
            {
                var dbparams = GetEncriptedParametersForUpdate(user);

                var result = _dapper.Execute("[dbo].[ACCT_UpdateUserProfile]", dbparams, CommandType.StoredProcedure);
                return Ok();
            }

            catch (Exception ex) {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost]
        [Route("updateUserPassword")]
        public IActionResult UpdateUserPassword(string userId, string newPassword)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("UserId", userId, DbType.String);
                dbparams.Add("Password", Encryptor.Encrypt(newPassword, _tripleDesKey), DbType.String);

                var result = _dapper.Execute("[dbo].[ACCT_UpdateUserPassword]", dbparams, CommandType.StoredProcedure);
                return Ok();
            }

            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }


        private DynamicParameters getParametersForUser(UserProfile user)          
        { 
            var dbparams = new DynamicParameters();
                    dbparams.Add("Oid", createId(), DbType.String);
                    dbparams.Add("UserId", user.UserId, DbType.String, direction: ParameterDirection.Output);
                    dbparams.Add("Username", user.Username, DbType.String);
                    dbparams.Add("FirstName", user.FirstName, DbType.String);
                    dbparams.Add("LastName", user.LastName, DbType.String);
                    dbparams.Add("Address", user.Address, DbType.String);
                    dbparams.Add("City", user.City, DbType.String);
                    dbparams.Add("Province", user.Province, DbType.String);
                    dbparams.Add("PostalCode", user.PostalCode, DbType.String);
                    dbparams.Add("Country", user.Country, DbType.String);
                    dbparams.Add("Phone", user.Phone, DbType.String);
                    dbparams.Add("Email", user.Email, DbType.String);
                    dbparams.Add("BirthDay", user.BirthDay, DbType.DateTime);
                    dbparams.Add("Gender", user.Gender, DbType.String);
                    dbparams.Add("SourceId", user.SourceId, DbType.String);
            dbparams.Add("Password", Encryptor.Encrypt(user.Password, _tripleDesKey), DbType.String);
            //dbparams.Add("Password", user.Password, DbType.String);               
            return dbparams;
        }

        private DynamicParameters getEncriptedParametersForUser(UserProfile user)
        {
            var dbparams = new DynamicParameters();
            dbparams.Add("Oid", createId(), DbType.String);
            dbparams.Add("UserId", user.UserId, DbType.String, direction: ParameterDirection.Output);
            dbparams.Add("Username", user.Username, DbType.String);
            dbparams.Add("FirstName", user.FirstName, DbType.String);
            dbparams.Add("LastName", user.LastName, DbType.String);
            dbparams.Add("Address", Encryptor.Encrypt(user.Address, _tripleDesKey), DbType.String);
            dbparams.Add("City", Encryptor.Encrypt(user.City, _tripleDesKey), DbType.String);
            dbparams.Add("Province", Encryptor.Encrypt(user.Province, _tripleDesKey), DbType.String);
            dbparams.Add("PostalCode", Encryptor.Encrypt(user.PostalCode, _tripleDesKey), DbType.String);
            dbparams.Add("Country", Encryptor.Encrypt(user.Country, _tripleDesKey), DbType.String);
            dbparams.Add("Phone", Encryptor.Encrypt(user.Phone, _tripleDesKey), DbType.String);
            dbparams.Add("Email", Encryptor.Encrypt(user.Email, _tripleDesKey), DbType.String);
            dbparams.Add("BirthDay", user.BirthDay, DbType.DateTime);
            dbparams.Add("Gender", user.Gender, DbType.String);
            dbparams.Add("SourceId", user.SourceId, DbType.String);
            dbparams.Add("Password", Encryptor.Encrypt(user.Password, _tripleDesKey), DbType.String);
            return dbparams;
        }

        private DynamicParameters GetEncriptedParametersForUpdate(UserProfile user) {

            var dbparams = new DynamicParameters();
            dbparams.Add("UserId", user.UserId, DbType.String);
            dbparams.Add("Address", Encryptor.Encrypt(user.Address, _tripleDesKey), DbType.String);
            dbparams.Add("Phone", Encryptor.Encrypt(user.Phone, _tripleDesKey), DbType.String);
            dbparams.Add("Email", Encryptor.Encrypt(user.Email, _tripleDesKey), DbType.String);
            dbparams.Add("City", Encryptor.Encrypt(user.City, _tripleDesKey), DbType.String);
            dbparams.Add("Province", Encryptor.Encrypt(user.Province, _tripleDesKey), DbType.String);
            dbparams.Add("PostalCode", Encryptor.Encrypt(user.PostalCode, _tripleDesKey), DbType.String);
            dbparams.Add("Country", Encryptor.Encrypt(user.Country, _tripleDesKey), DbType.String);           

            return dbparams;
        }

        private string createId()
        {
            Guid guid = Guid.NewGuid();
            return guid.ToString();
        }

        /*
        public static string EncryptTextToMemory(string Data, string tripleDesKey)
        {
            try
            {
                if (!string.IsNullOrEmpty(Data))
                {
                    byte[] key = System.Text.Encoding.UTF8.GetBytes(tripleDesKey);
                    // Create a MemoryStream.
                    MemoryStream mStream = new MemoryStream();
                    TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                    tdes.Key = key;
                    tdes.GenerateIV();
                    ICryptoTransform encryptor = tdes.CreateEncryptor();

                    // Create a CryptoStream using the MemoryStream
                    // and the passed key and initialization vector (IV).
                    CryptoStream cStream = new CryptoStream(mStream, encryptor,
                        CryptoStreamMode.Write);



                    // Convert the passed string to a byte array.
                    byte[] toEncrypt = new UTF8Encoding().GetBytes(Data);//Decode(Data);


                    var hexString = BitConverter.ToString(toEncrypt);
                    hexString = hexString.Replace("-", "");

                    toEncrypt = Decode(hexString);

                    // Write the byte array to the crypto stream and flush it.
                    cStream.Write(toEncrypt, 0, toEncrypt.Length);
                    cStream.FlushFinalBlock();

                    // Get an array of bytes from the
                    // MemoryStream that holds the
                    // encrypted data.
                    byte[] ret = mStream.ToArray();

                    // Close the streams.
                    cStream.Close();
                    mStream.Close();

                    var text = ByteArrayToString(ret);//Convert.ToBase64String(ret);

                    // Return the encrypted buffer.
                    return text;
                }
                else
                {

                    return "";
                }
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string ByteArrayToHexString(byte[] Bytes)
        {
            StringBuilder hex = new StringBuilder(Bytes.Length * 2);
            foreach (byte b in Bytes)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        static string Encrypt(string plainText, string tripleDesKey)
        {
            if (!string.IsNullOrEmpty(plainText))
            {
                byte[] encrypted;
                byte[] key = System.Text.Encoding.UTF8.GetBytes(tripleDesKey);
                // Create a new TripleDESCryptoServiceProvider.  
                using (TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider())
                {
                    byte[] toEncrypt = new UTF8Encoding().GetBytes(plainText);
                    tdes.Key = key;
                    tdes.Mode = CipherMode.ECB;
                    //tdes.Padding = PaddingMode.None;
                    tdes.GenerateIV();
                    // Create encryptor  
                    ICryptoTransform encryptor = tdes.CreateEncryptor();
                    // Create MemoryStream  
                    using (MemoryStream ms = new MemoryStream())
                    {
                        // Create crypto stream using the CryptoStream class. This class is the key to encryption  
                        // and encrypts and decrypts data from any given stream. In this case, we will pass a memory stream  
                        // to encrypt  
                        using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                        {
                            // Create StreamWriter and write data to a stream  
                            using (StreamWriter sw = new StreamWriter(cs))
                                sw.Write(plainText);
                            encrypted = ms.ToArray();
                        }
                    }
                }

                // Return encrypted data  
                var s = ByteArrayToString(encrypted);
                return s;
                //return ByteArrayToString(encrypted);
            }
            else
            {

                return "";
            }
        }

        public static string EncryptionMethod(string Text, string key)
        {
            string encryptedText = string.Empty;
            try
            {
                byte[] clearBytes = Encoding.UTF8.GetBytes(Text);//StringToByteArray(Text); //Encoding.UTF8.GetBytes(Text);
                TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
                des.Mode = CipherMode.ECB;
                des.GenerateIV();
                des.Key = Encoding.UTF8.GetBytes(key);   //Passing key in byte array
               //des.BlockSize = 64;
                //byte[] ivBytes = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
                //des.IV = ivBytes;
                ICryptoTransform ct = des.CreateEncryptor();   //Interface with some result
                byte[] resultArray = ct.TransformFinalBlock(clearBytes, 0, clearBytes.Length);
                encryptedText = ByteArrayToHexString(resultArray);
            }
            catch (Exception exception)
            {
                return "";
            }
            return encryptedText;

        }

        public static byte[] StringToByteArray2(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static string DecryptTextFromMemory(string Data)
        {
            try
            {
                byte[] bytes = StringToByteArray(Data);
                var keyArray = UTF8Encoding.UTF8.GetBytes(_tripleDesKey);
                // Create a new MemoryStream using the passed
                // array of encrypted data.
                MemoryStream msDecrypt = new MemoryStream(bytes);

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.GenerateIV();
                ICryptoTransform decryptor = tdes.CreateDecryptor();

                // Create a CryptoStream using the MemoryStream
                // and the passed key and initialization vector (IV).
                CryptoStream csDecrypt = new CryptoStream(msDecrypt,
                    decryptor,
                    CryptoStreamMode.Read);

                // Create buffer to hold the decrypted data.
                byte[] fromEncrypt = new byte[Data.Length];

                // Read the decrypted data out of the crypto stream
                // and place it into the temporary buffer.
                csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

                //Convert the buffer into a string and return it.
                return new UTF8Encoding().GetString(fromEncrypt);
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
        }

        static string Decrypt(string cipherText, string tripleDesKey)
        {

            string plaintext = null;
            byte[] bytes = StringToByteArray(cipherText);
            // Create TripleDESCryptoServiceProvider  
            using (TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider())
            {

                var keyArray = UTF8Encoding.UTF8.GetBytes(_tripleDesKey);

                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                //tdes.Padding = PaddingMode.None;
                tdes.GenerateIV();

                // Create a decryptor  
                ICryptoTransform decryptor = tdes.CreateDecryptor();
                // Create the streams used for decryption.  
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    // Create crypto stream  
                    using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        // cs.FlushFinalBlock();
                        // Read crypto stream  
                        using (StreamReader reader = new StreamReader(cs,Encoding.UTF8))
                        {
                            plaintext = reader.ReadToEnd();
                        }

                    }
                }
            }

            plaintext = plaintext.Replace("\b", "").Replace("\a", "").Replace("\u0001", "").Replace("\u0001", "").Replace("\u0002", "").Replace("\u0003", "").Replace("\u0004", "").Replace("\u0005", "").Replace("\u0006", "");

            return plaintext;
        }
        
        public static byte[] Decode(string s)
        {
            byte[] abyte0 = new byte[s.Length / 2];
            string s1 = s.ToLower();
            for (int i = 0; i < s1.Length; i += 2)
            {
                char c = s1.ElementAt(i);
                char c1 = s1.ElementAt(i + 1);
                int j = i / 2;
                if (c < 'a')
                    abyte0[j] = (byte)(c - 48 << 4);
                else
                    abyte0[j] = (byte)((c - 97) + 10 << 4);
                if (c1 < 'a')
                    abyte0[j] += (byte)(c1 - 48);
                else
                    abyte0[j] += (byte)((c1 - 97) + 10);
            }

            return abyte0;
        }

        public static byte[] ToBytes(string hex)
        {
            if (string.IsNullOrWhiteSpace(hex))
                throw new ArgumentException("Hex cannot be null/empty/whitespace");

            if (hex.Length % 2 != 0)
                throw new FormatException("Hex must have an even number of characters");

            bool startsWithHexStart = hex.StartsWith("0x", StringComparison.OrdinalIgnoreCase);

            if (startsWithHexStart && hex.Length == 2)
                throw new ArgumentException("There are no characters in the hex string");


            int startIndex = startsWithHexStart ? 2 : 0;

            byte[] bytesArr = new byte[(hex.Length - startIndex) / 2];

            char left;
            char right;

            try
            {
                int x = 0;
                for (int i = startIndex; i < hex.Length; i += 2, x++)
                {
                    left = hex[i];
                    right = hex[i + 1];
                    bytesArr[x] = (byte)((hexmap[left] << 4) | hexmap[right]);
                }
                return bytesArr;
            }
            catch (KeyNotFoundException)
            {
                throw new FormatException("Hex string has non-hex character");
            }
        }

        public static string Decryptor240815B(string Message) /* Working * /
        {
           
            string cipher = Message.Replace(" ", "+");
            byte[] keyBytes;
            //string cipherString = FromHexString(cipher);
            //byte[] cipherBytes = Convert.FromBase64String(cipher);

            byte[] cipherBytes = Decode(cipher);//UTF8Encoding.UTF8.GetBytes(cipher);
            //byte[] cipherBytes = Encoding.Default.GetBytes(cipher);

            keyBytes = UTF8Encoding.UTF8.GetBytes(_tripleDesKey);

            var tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyBytes;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.None;

            ICryptoTransform transformation = tdes.CreateDecryptor();
            byte[] decryptedBytes = transformation.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
            tdes.Clear();

            string response = System.Text.Encoding.UTF8.GetString(decryptedBytes);
            //var test = FromHexString(response);
            return response;
        }

        public static byte[] FromHexString(string hexString)
        {
            var bytes = new byte[hexString.Length / 2];
            for (var i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }

            return bytes;
        }


        public static byte[] StringToByteArrayFastest(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            //return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        //public static byte[] StringToByteArray(string hex)
        //{
        //    return Enumerable.Range(0, hex.Length)
        //                     .Where(x => x % 2 == 0)
        //                     .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
        //                     .ToArray();
        //}

        */

    }
}