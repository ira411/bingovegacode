﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BingoVega.Services;
using BingoVega.Models;
using Dapper;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System;
using System.Data;
using BingoVega.Security;
using BingoVega.API.ViewModels;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Dynamic;

namespace BingoVega.API.Controllers
{
    [Route("api/bank")]
    [ApiController]
    public class BankController : ControllerBase
    {
        private readonly IDapper _dapper;
        private IConfiguration _config;
        private const string tripleDesKey = "58d8b19df78558d2d0e5f509";

        public BankController(IDapper dapper, IConfiguration config)
        {
            _dapper = dapper;
            _config = config;
        }

        [HttpGet]
        [Route("getCards")]
        public IActionResult GetCreditCards(string userid)
        {
            try
            {                
                var dbparams = new DynamicParameters();
                dbparams.Add("@UserId", userid);
                var cards = _dapper.GetAll<CreditCardEncripted>("getCreditCardsByUserId", dbparams, System.Data.CommandType.StoredProcedure);            
            
                return Ok(cards);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet]
        [Route("getCard")]
        public IActionResult GetCreditCard(int id, string userId)
        {
            try
            {                
                var dbparams = new DynamicParameters();
                dbparams.Add("@Id", id);
                dbparams.Add("@UserId", userId);
                var card = _dapper.Get<CreditCardEncripted>("getCreditCardById", dbparams, System.Data.CommandType.StoredProcedure);

                return Ok(card);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("getTransactionNumber")]
        public ActionResult GetTransactionNumber()
        {            
            try
            {
                var dbparams = new DynamicParameters ();
                dbparams.Add(
                    name: "@transaction",
                    dbType: DbType.Int32,
                    direction: ParameterDirection.ReturnValue
                    );
                var result = _dapper.Execute("[dbo].[getNextTNumber]", dbparams, CommandType.StoredProcedure);
                int transaction = dbparams.Get<int>("@transaction");

                return Ok(transaction);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [HttpGet]
        [Route("getPaymentMethods")]
        public IActionResult GetPaymentMethods(int TypeOfTransaction)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("@TypeOfTransaction", TypeOfTransaction);
                var cards = _dapper.GetAll<object>("AR_GetPaymentMethods", dbparams, CommandType.StoredProcedure);

                return Ok(cards);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost("deleteCreditCard")]
        public IActionResult DeleteCreditCard(CreditCard card)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("@Id", card.Id);
                dbparams.Add("@UserId", card.UserId);

                var result = _dapper.Execute("[dbo].[DeleteCreditCard]", dbparams, CommandType.StoredProcedure);
                return Ok();
            }

            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost("addCreditCard")]
        public IActionResult AddCreditCard(CreditCard card)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("@accountid", card.UserId);
                dbparams.Add("@ccnumber", Encryptor.Encrypt(card.CardNumber.ToString(), tripleDesKey)); //
                dbparams.Add("@expmonth", Encryptor.Encrypt(card.ExpiryMonth.ToString(), tripleDesKey));//
                dbparams.Add("@expyear", Encryptor.Encrypt(card.ExpiryYear.ToString(), tripleDesKey));//
                dbparams.Add("@first", card.FirstName);
                dbparams.Add("@last", card.LastName);
                dbparams.Add("@middle", card.MiddleName);
                dbparams.Add("@CRCD_address", Encryptor.Encrypt(card.Address, tripleDesKey));//
                dbparams.Add("@CRCD_city", Encryptor.Encrypt(card.City, tripleDesKey));//
                dbparams.Add("@CRCD_state", Encryptor.Encrypt(card.Province, tripleDesKey));//
                dbparams.Add("@CRCD_zip", Encryptor.Encrypt(card.Postalcode, tripleDesKey));//
                dbparams.Add("@CRCD_country", Encryptor.Encrypt(card.Country, tripleDesKey));//
                dbparams.Add("@CRCD_phone", Encryptor.Encrypt(card.Phone, tripleDesKey));//
                dbparams.Add("@PaymentTypeID", card.CardType);

                var result = _dapper.Execute("[dbo].[InsertCreditCard]", dbparams, CommandType.StoredProcedure);
                return Ok();
            }

            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost("editCreditCard")]
        public IActionResult EditCreditCard(CreditCard card)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("@id", card.Id);
                dbparams.Add("@accountid", card.UserId);
                dbparams.Add("@ccnumber", Encryptor.Encrypt(card.CardNumber.ToString(), tripleDesKey)); //
                dbparams.Add("@expmonth", Encryptor.Encrypt(card.ExpiryMonth.ToString(), tripleDesKey));//
                dbparams.Add("@expyear", Encryptor.Encrypt(card.ExpiryYear.ToString(), tripleDesKey));//
                dbparams.Add("@first", card.FirstName);
                dbparams.Add("@last", card.LastName);
                dbparams.Add("@middle", card.MiddleName);
                dbparams.Add("@CRCD_address", Encryptor.Encrypt(card.Address, tripleDesKey));//
                dbparams.Add("@CRCD_city", Encryptor.Encrypt(card.City, tripleDesKey));//
                dbparams.Add("@CRCD_state", Encryptor.Encrypt(card.Province, tripleDesKey));//
                dbparams.Add("@CRCD_zip", Encryptor.Encrypt(card.Postalcode, tripleDesKey));//
                dbparams.Add("@CRCD_country", Encryptor.Encrypt(card.Country, tripleDesKey));//
                dbparams.Add("@CRCD_phone", Encryptor.Encrypt(card.Phone, tripleDesKey));//
                dbparams.Add("@PaymentTypeID", card.CardType);

                var result = _dapper.Execute("[dbo].[UpdateCreditCard]", dbparams, CommandType.StoredProcedure);
                return Ok();
            }

            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost("beginTransaction")]
        public IActionResult BeginDepositTransaction(string userId, double amount, string cctype, int cc_id, int paymenttypeid)
        {
            try
            {                 
                var dbparams = new DynamicParameters();
                dbparams.Add("@accountid", userId);
                dbparams.Add("@amount", amount);
                dbparams.Add("@payCatId", 10034);
                dbparams.Add("@cctype", cctype);
                dbparams.Add("@cc_id", cc_id);
                dbparams.Add("@paymenttypeid", paymenttypeid);
                dbparams.Add("@ccnumber", "");
                dbparams.Add("@tnumber", 0, direction: ParameterDirection.Output);
                dbparams.Add("@stamp", "");
                dbparams.Add("@rettext", "");


                var result = _dapper.Execute("[dbo].[AR_InitialTransaction]", dbparams, CommandType.StoredProcedure);
                int transaction = dbparams.Get<int>("tnumber");
                return Ok(transaction);
            }

            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost("postBackUrl")]
        [Consumes("application/x-www-form-urlencoded")]
        public IActionResult ConfirmationOfTransaction([FromForm] IFormCollection data)
        {

            try
            {

                string type = data["type"];
                string accountId = data["accountId"];
                string transactionId = data["transactionId"];
                string gatewayTransactionId = data["gatewayTransactionId"];
                string gatewayTransactionNote = data["gatewayTransactionNote"];
                string gatewayTransactionStatus = data["gatewayTransactionStatus"];
                //int gatewayAuthorization = 0;
                string gatewayProcessorDescription = data["gatewayProcessorDescription"];
                string amount = data["amount"];
                string status = data["status"];

                var dbparams = new DynamicParameters();
                dbparams.Add("@transactionId", transactionId);
                dbparams.Add("@accountId", accountId);
                var parameter = _dapper.Get<TransactionParameter>("[dbo].[getUserNameAndCreditCard]", dbparams, CommandType.StoredProcedure);

                dbparams = new DynamicParameters();
                dbparams.Add("@tnumber", transactionId);
                dbparams.Add("@ccnumber", parameter.CreditCard);
                dbparams.Add("@VendTXNNumber", transactionId);
                dbparams.Add("@VendAuthCode", "");
                dbparams.Add("@VendAuthTime", "");
                dbparams.Add("@VendStatusCode", gatewayTransactionStatus);
                dbparams.Add("@VendAuthString", gatewayProcessorDescription);
                dbparams.Add("@reference", gatewayProcessorDescription);
                dbparams.Add("@Status", gatewayTransactionStatus);
                dbparams.Add("@amount", double.Parse(amount));
                dbparams.Add("@accountid", accountId);
                dbparams.Add("@username", parameter.UserName);
                var result = _dapper.Execute("[dbo].[AR_CommitTransaction]", dbparams, CommandType.StoredProcedure);

                return Ok("status=1&transactionId=&gatewayTransactionId=");
            }

            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost("createWithdrawalRequest")]        
        public IActionResult CreateWithdrawalRequest([FromBody] dynamic model)
        {            
            try
            {
                dynamic data = JsonConvert.DeserializeObject<dynamic>(model.ToString());
                
                var dbparams = new DynamicParameters();
                dbparams.Add("@accountid", (string)data.accountId);
                dbparams.Add("@amount", (int)data.amount);
                dbparams.Add("@paycatid", (int)data.paymentMethod);

                dbparams.Add("@VendorName", "");
                dbparams.Add("@tnumber", 0);
                dbparams.Add("@retval", 0, direction: ParameterDirection.Output);
                dbparams.Add("@retstat1", "");
                
                _dapper.Execute("[dbo].[AP_PayoutProcess]", dbparams, CommandType.StoredProcedure);
                                
                int result = dbparams.Get<int>("@retval"); // -1 fail 0 good
                bool isSuccesful = (result == 0) ? true : false;
                return Ok(isSuccesful);
            }

            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpGet]
        [Route("getPendingWithdrawal")]
        public IActionResult GetPendingWithdrawal(string userId)
        {
            try
            {
                var dbparams = new DynamicParameters();
                dbparams.Add("@UserId", userId);
                var withdrawal = _dapper.Get<object>("AP_GetPendingWithdrawal", dbparams, CommandType.StoredProcedure);

                return Ok(withdrawal);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }


        [HttpPost("cancelWithdrawalRequest")]
        public IActionResult CancelWithdrawalRequest([FromBody] dynamic model)
        {
            try
            {
                dynamic data = JsonConvert.DeserializeObject<dynamic>(model.ToString());

                var dbparams = new DynamicParameters();
                dbparams.Add("@tnumber", (int)data.transaction); //5924552,
                dbparams.Add("@status", "SN");//'SN',
                dbparams.Add("@accountid", (string)data.accountId); //'BVA1858130',

                dbparams.Add("@amount", (double)data.amount);  //10,
                dbparams.Add("@note", "Used for Deposit"); //'Used for Deposit',
                dbparams.Add("@extranotes", "");//' ',

                dbparams.Add("@confnoteid", "PR");//'PR',
                dbparams.Add("@confnote", "");//' ',
                dbparams.Add("@@sourceid", 0);//0,

                dbparams.Add("@paycatid", (int)data.paymentMethodId); //29,
                dbparams.Add("@cctranid", ""); //'0',
                dbparams.Add("@retval", "", direction: ParameterDirection.Output);
                dbparams.Add("@retval1", "");

            
                _dapper.Execute("[dbo].[AR_Redemption_Flowback]", dbparams, CommandType.StoredProcedure);

                string result = dbparams.Get<string>("@retval"); // -1 fail 0 good
                bool isSuccesful = (result == "OK") ? true : false;
                return Ok(isSuccesful);
            }

            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

    }
}
