﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.ViewModels
{
    public class UserProfileViewModel
    {
       
        public string Oid { get; set; }
        public string FirstName { get; set; }
        public string Username { get; set; }
        public string ClassName { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Language { get; set; }
        public string Active { get; set; }
        public string Activated { get; set; }
        public string Question { get; set; }
        public string Hint { get; set; }
        public string Answer { get; set; }
        public DateTime? BirthDay { get; set; }
        public int SourceId { get; set; }
        public string Gender { get; set; }
        public string ReceiveBroadCast { get; set; }

        public decimal? Balance { get; set; }
        public decimal? Winnings { get; set; }
        public decimal? Bank { get; set; }
        public decimal? Cash { get; set; }
        public decimal? Bonus { get; set; }
        public bool HasFullLobbyAccess { get; set; }
        public int CreditCardsAllowed { get; set; }
        public int DepositsLastMonth { get; set; }
        public double MaxBalanceToDeposit { get; set; }

        public string Token { get; set; }
    }
}
