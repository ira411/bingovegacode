﻿namespace BingoVega.API.ViewModels
{
    public struct TransactionParameter
    {

        public string Type { get; set; }
        public string AccountId { get; set; }
        public string TransactionId { get; set; }
        public string GatewayTransactionId { get; set; }
        public string GatewayTransactionNote { get; set; }
        public string GatewayTransactionStatus { get; set; }
        public string GatewayAuthorization { get; set; }
        public string GatewayProcessorDescription { get; set; }       
        public string Amount { get; set; }
        public string Status { get; set; }



        public string CreditCard { get; set; }
        public string UserName { get; set; }


    }
}
