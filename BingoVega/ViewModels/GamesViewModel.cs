﻿using BingoVega.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.API.ViewModels
{
    public class GamesViewModel
    {
        public List<Suite> NgGames { get; set; }
        public List<GamePs> PsGames { get; set; }
    }
}
