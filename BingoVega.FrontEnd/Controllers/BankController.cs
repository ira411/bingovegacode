﻿using BingoVega.FrontEnd.Models;
using BingoVega.FrontEnd.Utils;
using BingoVega.Models;
using BingoVega.Security;
using BingoVega.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Controllers
{
    public class BankController : Controller
    {

        private const string _tripleDesKey = "58d8b19df78558d2d0e5f509";

        private readonly ILogger<BankController> _logger;
        private IConfiguration _config;

        public BankController(ILogger<BankController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public IActionResult DepositNow()
        {
            return View();
        }
               
        public IActionResult Banking()
        {
            string userId = Session.getCurrentUserId(HttpContext);
            BankViewModel vm = new BankViewModel();
            vm.Amounts = getAmounts();

            if(Session.getUserCash(HttpContext) != null)
            {
                vm.Total = Double.Parse(Session.getUserCash(HttpContext));
            }
            else
            {
                vm.Total = 0.00;
            }
            if(Session.getUserBonus(HttpContext) != null)
            {
                vm.Bonus = Double.Parse(Session.getUserBonus(HttpContext));
            }
            else
            {
                vm.Bonus = 0.00;
            }
            vm.Available = (vm.Total < 200) ? 0 : vm.Total;
            vm.DepositsLastMonth = Session.getUserDeposits(HttpContext);            
            vm.PaymentMethods = Api.GetPaymentMethods(2, _config);
            vm.Withdrawal = Api.GetPendingWithdrawal(userId, _config);            
            vm.HasWithdrawal = (vm.Withdrawal==null) ? false : true;
            if (!vm.HasWithdrawal)
            {
                vm.Withdrawal = new Withdrawal();
                vm.Withdrawal.TransactionNumber = 0;
                vm.Withdrawal.Amount = 0;
                vm.Withdrawal.PaymentMethodId = 0;
            }
            double maxBalance = Session.getMaxBalanceToDeposit(HttpContext);
            vm.BalanceExceedsMax = (vm.Total > maxBalance) ? true : false;
            return View(vm);
        }
        
        public IActionResult SelectCreditCard(DepositNowViewModel data)
        {

            
            string userid = Session.getCurrentUserId(HttpContext);
            data.CreditCards = Api.GetCreditCards(userid, _config);
            if (data.Amount == 0)
            {
                data.Amount = Session.getAmountToDeposit(HttpContext);
            }
            else {
                Session.saveAmountToDeposit(HttpContext, data.Amount.ToString());
            }
            
            return View(data);
        }
        
        [HttpPost()]
        [ActionName("deposit")]
        public IActionResult DepositMoney(int id, int code, string amount)
        {
            try
            {
                string userid = Session.getCurrentUserId(HttpContext);
                string userEmail = Session.getUserEmail(HttpContext);
                CreditCard card = Api.GetCreditCard(id, userid, _config);                
                int transaction = Api.BeginDepositTransaction(userid, amount, card.CardTypeDescription, card.Id, card.CardType, _config);
                string rootUrl = _config.GetSection("SiteUrl").GetSection("url").Value;
                string apiRootUrl = _config.GetSection("ApiConfig").GetSection("url").Value;               
                string url = string.Format("https://cashier.bngvg.com/vegacredit/?" +
                    "transactionId={0}&amount={1}&userEmail={2}&accountId={3}&cardFirstName={4}&" +
                    "cardMiddleName={5}&cardLastName={6}&userPostalCode={7}&cardNumber={8}&" +
                    "cardExpiryMonth={9}&cardExpiryYear={10}&cardVerificationValue={11}&redirectUrl={12}LobbyGames/BingoRooms" +
                    "&postBackURL={13}/Bank/postBackURL",
                    transaction,
                    amount,
                    userEmail,
                    userid,
                    card.FirstName,
                    "",
                    card.LastName,
                    card.Postalcode, 
                    card.CardNumber, 
                    card.ExpiryMonth,
                    card.ExpiryYear,
                    code,
                    rootUrl,
                    apiRootUrl
                    );
                
                return Json(new { redirectToUrl = Url.Action("ProcessDeposit", "Bank", new { ProcessDeposit = url }) });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public IActionResult ProcessDeposit(string ProcessDeposit)
        {            
            return View("ProcessDeposit", ProcessDeposit);
        }

        public IActionResult DeleteCard(int id)
        {
            try
            {
                string userid = Session.getCurrentUserId(HttpContext);
                Api.DeleteCreditCard(new CreditCard() { 
                Id= id,
                UserId= userid,
                }, _config);

                return RedirectToAction("SelectCreditCard", "Bank");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public IActionResult AddCreditCard(int id)
        {
            CreditCardViewModel c = new CreditCardViewModel();
            c.card = new CreditCard();                     
            c.CrediCardTypes = new List<CreditCardType>()
            { 
                new CreditCardType(){ Id=2, Type="Visa"},
                new CreditCardType(){ Id=3, Type="MasterCard"},
            };
            c.Countries = Api.GetCountries(_config);


            return View("CreditCardForm", c);
        }

        public IActionResult EditCreditCard(int id)
        {
            CreditCardViewModel c = new CreditCardViewModel();
            string userid = Session.getCurrentUserId(HttpContext);
            c.card = Api.GetCreditCard(id, userid, _config);
            c.CrediCardTypes = new List<CreditCardType>()
            {
                new CreditCardType(){ Id=2, Type="Visa"},
                new CreditCardType(){ Id=3, Type="MasterCard"},
            };            
            c.Countries = Api.GetCountries(_config);
            c.ExpiredDate = c.card.ExpiryMonth + "/" + c.card.ExpiryYear;

            return View("CreditCardForm", c);
        }

        [HttpPost()]
        [ActionName("withdrawalRequest")]
        public IActionResult WithdrawalRequest(double WithdrawalAmount, int paymentMethod)
        {
            try
            {
                string userid = Session.getCurrentUserId(HttpContext);
                bool result = Api.CreateWithdrawalRequest(_config, userid, WithdrawalAmount, paymentMethod);

                if (result)
                {
                    return Json(new { result = result, redirectToUrl = Url.Action("Banking", "Bank") });
                }
                else
                {
                    return Json(new { result = result });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }



        }

        public IActionResult Echeck(decimal EcheckAmount)
        {
            string username = Session.getCurrentUser(HttpContext);
            UserProfileViewModel user = Api.GetUserProfile(_config, username);


            string userid = Session.getCurrentUserId(HttpContext);
            string userEmail = Session.getUserEmail(HttpContext);
            string userName = Session.getCurrentUser(HttpContext);
            string postalcode = Session.getUserPostalCode(HttpContext);
            int transaction = Api.GetTransactionNumber(_config);
            string rootUrl = _config.GetSection("SiteUrl").GetSection("url").Value;
            string apiRootUrl = _config.GetSection("ApiConfig").GetSection("url").Value;
            string url = string.Format("https://cashier.bngvg.com/echeck/?" +
                    "accountId={0}&transactionId={1}&amount={2}&userEmail={3}&userFirstName={4}&" +
                    "userLastName={5}&userPhoneNumber={6}&userAddress={7}&userCity={8}&userPostalCode={9}&"+
                    "userCountry={10}&userProvince={11}&postBackURL={12}/Bank/postBackURL",
                    user.UserId,
                    transaction,
                    EcheckAmount,
                    user.Email,
                    user.FirstName,
                    user.LastName,
                    user.Phone,
                    user.Address,
                    user.City,
                    user.PostalCode,
                    user.Country,
                    user.Province,
                    apiRootUrl
                    );


            return View("Echeck", url);
        }

        [HttpPost()]
        [ActionName("Register")]
        public IActionResult Save(CreditCard card)
        {            
            try
            {
                card.UserId = Session.getCurrentUserId(HttpContext);

                if (card.Id == 0)
                {
                    Api.AddCreditCard(_config, card);
                    return Json(new { redirectToUrl = Url.Action("SelectCreditCard", "Bank") });
                }
                else
                {
                    Api.EditCreditCard(_config, card);
                    return Json(new { redirectToUrl = Url.Action("SelectCreditCard", "Bank") });
                }
                
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
        }

        [HttpPost()]
        [ActionName("cancelWithdrawal")]
        public IActionResult CancelWithdrawal(int transaction, double amount, int paymentMethod)
        {
            try
            {
                string userid = Session.getCurrentUserId(HttpContext);                
                bool result = Api.CancelWithdrawalRequest(_config, transaction, userid, amount, paymentMethod);

                if (result)
                {
                    return Json(new { result = result, redirectToUrl = Url.Action("Banking", "Bank") });
                }
                else
                {
                    return Json(new { result = result });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }



        }
        private List<decimal> getAmounts()
        {
            Random r = new Random();
            int[] nb = new int[] { 25, 50, 75, 100, 200, 300, 350 };

            List<decimal> l = new List<decimal>();
            foreach (int n in nb)
            {
                l.Add(n + ((decimal)(r.Next(0, 99) + 1) / 100));
            }
            return l;
        }
        

    }
}
