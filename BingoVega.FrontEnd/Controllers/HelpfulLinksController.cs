﻿using BingoVega.FrontEnd.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Controllers
{
    public class HelpfulLinksController : Controller
    {
        private readonly ILogger<HelpfulLinksController> _logger;

        public HelpfulLinksController(ILogger<HelpfulLinksController> logger)
        {
            _logger = logger;
        }

        public IActionResult HouseRules()
        {
            return View();
        }
        public IActionResult InternetAbuse()
        {
            return View();
        }
        public IActionResult Requirements()
        {
            return View();
        }
        public IActionResult SiteMap()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
