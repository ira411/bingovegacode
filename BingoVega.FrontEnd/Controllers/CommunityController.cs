﻿using BingoVega.FrontEnd.Models;
using BingoVega.FrontEnd.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Controllers
{
    public class CommunityController : Controller
    {
        private readonly ILogger<CommunityController> _logger;

        public CommunityController(ILogger<CommunityController> logger)
        {            
            _logger = logger;
        }

        public IActionResult Gallery()
        {
            return View();
        }
        public IActionResult LatestWinners()
        {
            return View();
        }
        public IActionResult Newsletter()
        {
            return View();
        }
        public IActionResult Testimonials()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
