﻿using BingoVega.FrontEnd.Models;
using BingoVega.FrontEnd.Utils;
using BingoVega.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace BingoVega.FrontEnd.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IConfiguration _config;

        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        } 
        
        public IActionResult Index(string affid, string cxd)
        {           
            string ip = DataPrepare.getRouterIpAddress();           
            saveIp(ip);
            saveCountry(ip);
            saveCxdAndAffid(affid, cxd);
           
            return View();
        }     

        private void saveCxdAndAffid(string affid, string cxd)
        {
            if (!String.IsNullOrEmpty(cxd))
            {
                if (DataPrepare.isAValidCxd(cxd))
                {
                    Utils.Cookie.save(Response, "cxd", cxd);

                    String affiliate = DataPrepare.getAffid(affid, cxd);
                    Utils.Cookie.save(Response, "affiliateid", affiliate);
                }
            }
        }

        private void saveIp(String ip)
        {
            if (!String.IsNullOrEmpty(ip)) Utils.Cookie.save(Response, "ipCliente", ip);
        }

        private void saveCountry(string ip)
        {
            string country = DataPrepare.getCountryByIpAddress(ip);
            if (!String.IsNullOrEmpty(country)) Utils.Cookie.save(Response, "country", country);
        }

         public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Promotions()
        {
            return View();
        }

        public IActionResult DepositBonuses()
        {
            return View();
        }

        public IActionResult RulerGalaxy()
        {
            return View();
        }

        public IActionResult Loyalty()
        {            
            return View();
        }

        public IActionResult ReferFriend()
        {
            return View();
        }

        public IActionResult CasinoCashBack()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
