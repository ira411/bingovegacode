﻿using BingoVega.FrontEnd.Models;
using BingoVega.FrontEnd.Utils;
using BingoVega.Models;
using BingoVega.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace BingoVega.FrontEnd.Controllers
{
    public class SignUpController : Controller
    {
        private readonly ILogger<LobbyGamesController> _logger;
        private IConfiguration _config;

        public SignUpController(ILogger<LobbyGamesController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public IActionResult SignUp()
        {
            ViewBag.Message = true;

            UserProfileViewModel s = new UserProfileViewModel();
            s.Countries= Api.GetCountries(_config);           
            
            return View(s);
        }

        public IActionResult Thanks()
        {
            ViewBag.Message = true;            
            return View();
        }

        [HttpPost()]
        [ActionName("Register")]
        public IActionResult Save(UserProfileViewModel newUser)
        {
            UserProfile user = getUserFromViewModel(newUser);
            string userId = Api.AddUser(Request, _config, user);
            if (!string.IsNullOrEmpty(userId))
            {
                Api.callCxd(Request, userId);                                    
                Session.saveCurrentUserId(HttpContext, user.UserId);
                Session.saveCurrentUser(HttpContext, user.Username);
                Session.saveNameCurrentUser(HttpContext, user.FirstName);
                Session.saveIsUserLogedIn(HttpContext, true);
                Session.saveUserBalance(HttpContext, "0");              
                return Json(new { redirectToUrl = Url.Action("Thanks", "SignUp") });
                
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private UserProfile getUserFromViewModel(UserProfileViewModel newUser)
        {
            UserProfile user = new UserProfile();
            user.FirstName = newUser.FirstName;
            user.Username = newUser.Username;
            user.Country =  newUser.Country;
            user.PostalCode = newUser.PostalCode;
            user.Province = (newUser.Country=="CANADA" || newUser.Country == "UNITED KINGDOM" || newUser.Country == "UNITED STATES ") ?newUser.ProvinceDropDown : newUser.Province;
            user.City = newUser.City;
            user.Address = newUser.Address;
            user.LastName = newUser.LastName;
            user.Phone = newUser.Phone;
            user.Email = newUser.Email;
            user.Password = newUser.Password;
            user.UserId = newUser.UserId;

            if (newUser.BirthDay != null)
            {
                user.BirthDay = DateTime.Parse(newUser.BirthDay);
            }

            user.SourceId = 0;
            user.Gender = newUser.Gender;
            user.UserId = "";

            return user;
        }

       
    }
}
