﻿using BingoVega.FrontEnd.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Controllers
{
    public class BingoController : Controller
    {
        private readonly ILogger<BingoController> _logger;

        public BingoController(ILogger<BingoController> logger)
        {
            _logger = logger;
        }

        public IActionResult RoomSchedule()
        {
            return View();
        }
        public IActionResult ChatFaq()
        {
            return View();
        }
        public IActionResult ChatGames()
        {
            return View();
        }
        public IActionResult Chatiquette()
        {
            return View();
        }
        public IActionResult ChatLingo()
        {
            return View();
        }
        public IActionResult ChatModerators()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {//
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
