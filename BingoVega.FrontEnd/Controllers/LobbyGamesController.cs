﻿using BingoVega.FrontEnd.Models;
using BingoVega.FrontEnd.Utils;
using BingoVega.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Controllers
{
    public class LobbyGamesController : Controller
    {
        private readonly ILogger<LobbyGamesController> _logger;
        private IConfiguration _config;

        public LobbyGamesController(ILogger<LobbyGamesController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public IActionResult SlotsGames()
        {            

            ViewBag.Message = true;
            BingoRoomViewModel room = new BingoRoomViewModel();
            room.TextForMarquee = Api.GetTextforBingoRoomMarquee();
            return View(room);
        }

        public IActionResult VideoPokerGames()
        {
            ViewBag.Message = true;
            BingoRoomViewModel room = new BingoRoomViewModel();
            room.TextForMarquee = Api.GetTextforBingoRoomMarquee();
            return View(room);
        }

        public IActionResult BingoRooms()
        {
            BingoRoomViewModel room = new BingoRoomViewModel();
            room.HasFullLobbyAccess = Api.GetLobbyAccessLevel(_config, HttpContext);
            room.Rooms =  Api.GetBingoRooms();
            room.TextForMarquee = Api.GetTextforBingoRoomMarquee();
            room.UserId = Session.getCurrentUserId(HttpContext); 
            (room.Token, room.JsessionId) = Api.GetUserCredentialsForBingoRooms(room.UserId);
            return View(room);
        }

        public IActionResult PreOrderCards(string roomId)
        {//test
            BingoRoomViewModel room = new BingoRoomViewModel();    
            string userId = Session.getCurrentUserId(HttpContext);           
            (string token, string jsessionId) = Api.GetUserCredentialsForBingoRooms(userId);
            room.UrlForPreOrderCards= string.Format("https://play.bngvg.com/preorder/{0}/{1}/{2}/{3}", roomId, userId, token, jsessionId);
            return View(room);
        }
               
    }
}
