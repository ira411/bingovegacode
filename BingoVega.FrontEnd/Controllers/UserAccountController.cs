﻿using BingoVega.FrontEnd.Models;
using BingoVega.FrontEnd.Utils;
using BingoVega.Models;
using BingoVega.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Controllers
{
    public class UserAccountController : Controller
    {
        private readonly ILogger<UserAccountController> _logger;
        private IConfiguration _config;

        public UserAccountController(ILogger<UserAccountController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public IActionResult UserProfile()
        {
            string username = Session.getCurrentUser(HttpContext);
            UserProfileViewModel user = Api.GetUserProfile(_config, username);
            return View(user);
        }

        public IActionResult UserProfileEdit()
        {
            string username = Session.getCurrentUser(HttpContext);
            UserProfileViewModel user = Api.GetUserProfile(_config, username);
            user.Password= string.Empty;
            return View(user);
        }

        [HttpPost]
        public IActionResult EditUserProfile(UserProfileViewModel user) {


            UserProfile profile = new UserProfile();
            profile.UserId = user.UserId;
            profile.Address = user.Address;
            profile.Phone = user.Phone;
            profile.Email = user.Email;
            profile.City = user.City;
            profile.Province = user.Province;
            profile.PostalCode = user.PostalCode;
            profile.Country = user.Country;           
            Api.UpdateUser(_config, profile);

            return RedirectToAction("UserProfile");
        }

        [HttpPost()]
        [ActionName("updatePassword")]
        public IActionResult EditUserPassword(string password)
        {
            string userId = Session.getCurrentUserId(HttpContext);
            Api.UpdatePassword(_config, userId, password);            
            return Json(new { redirectToUrl = Url.Action("UserProfileEdit", "UserAccount") });
        }

        public ActionResult UserLogin(string username, string password)
        {
            bool  isLoginValid= Api.AuthenticateUser(_config, username, password);

            if (isLoginValid)
            {
                UserProfileViewModel user = Api.GetUserProfile(_config, username);
                if (user != null)
                {
                    Session.saveCurrentUserId(HttpContext, user.UserId);
                    Session.saveCurrentUser(HttpContext, user.Username);
                    Session.saveNameCurrentUser(HttpContext, user.FirstName);
                    Session.saveIsUserLogedIn(HttpContext, true);
                    Session.saveUserBalance(HttpContext, user.Balance.ToString());
                    Session.saveUserCash(HttpContext, user.Cash.ToString());
                    Session.saveUserBonus(HttpContext, user.Bonus.ToString());
                    Session.saveUserDeposits(HttpContext, user.DepositsLastMonth);
                    Session.saveUserEmail(HttpContext, user.Email);
                    Session.saveUserPostalCode(HttpContext, user.PostalCode);
                    Session.saveMaxBalanceToDeposit(HttpContext, user.MaxBalanceToDeposit.ToString());
                    return RedirectToAction("BingoRooms", "LobbyGames");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                    

                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult LoadUserLogin(string username)
        {            
            UserProfileViewModel user = Api.GetUserProfile(_config, username);

            if (user != null)
            {
                Session.saveCurrentUser(HttpContext, user.Username);
                Session.saveNameCurrentUser(HttpContext, user.FirstName);
                Session.saveIsUserLogedIn(HttpContext, true);
                return RedirectToAction("BingoRooms", "LobbyGames");
            }
            else
            {   
                return RedirectToAction("Index", "Home");
            }
        }

        public IActionResult Logout()
        {
            Session.closeSession(HttpContext);
            return RedirectToAction("Index", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
