﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Utils
{
    public static class Cookie
    {

        public static void save(HttpResponse response, string cookiefield, string value)
        {
            CookieOptions c = new CookieOptions();
            c.Expires = DateTime.Now.AddDays(1);
            response.Cookies.Append(cookiefield, value, c);
        }

        public static string getValue(HttpRequest request, string cookiefield)
        {
            return request.Cookies.TryGetValue(cookiefield, out string value)  ? value : "";
        }

    }
}
