﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Utils
{
    public static class DataPrepare
    {

        public static string prepareAffiliateid(String cxd)
        {
            int endOfAffidPart = cxd.IndexOf("_");
            string affid = cxd.Substring(0, endOfAffidPart);
            return affid;
        }

        public static bool isAValidCxd(string cdx) {
            Regex regex = new Regex(@"^[a-zA-Z0-9_]*$");
            bool a = regex.IsMatch(cdx,0);                       
            return a;
        
        }

        public static string getAffid(string affid, string cxd)
        {            
            if (String.IsNullOrEmpty(affid))
            {
                return prepareAffiliateid(cxd);
            }
            else return affid;
        }

        //Sin uso
        public static string getLocalIpAddress(HttpRequest request)
        {
            IPAddress remoteIpAddress = request.HttpContext.Connection.RemoteIpAddress;
            string result = "";
            if (remoteIpAddress != null)
            {               
                if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                {
                    remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
                        .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                }
                result = remoteIpAddress.ToString();                
            }
            return result;
        }

        public static string getRouterIpAddress()
        {
            string result = "";
            WebClient c = new WebClient();
            string[] sources =
            {
                "https://api.ipify.org/",
                "http://icanhazip.com/",
                "http://ipinfo.io/ip",
                "https://checkip.amazonaws.com/",
                "https://wtfismyip.com/text"
            };

            for (int a = 0; a < sources.Length; a++)
            {
                try {
                    result = new WebClient().DownloadString(sources[a]).Replace("\n", "");
                    if (isAValidIpAddress(result)) break;
                }
                catch(Exception ex){ result = ""; }
            }

            return result;
        }

        public static string getCountryByIpAddress(string ip)
        {
            string key = "8dde632391b9cc22df72ca0f4c110615";
            var url = $"http://api.ipstack.com/{ip}?access_key={key}";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync(url).Result;
            string country = "";
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync().Result;
                dynamic obj = JsonConvert.DeserializeObject(jsonString);
                country = obj.country_code;
            }

            return country;
        }

        private static bool isAValidIpAddress(string ip)
        {
            IPAddress IP;
            bool isValid = IPAddress.TryParse(ip, out IP);
            return isValid;
        }

       

    }
}
