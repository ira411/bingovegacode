﻿using BingoVega.FrontEnd.Models;
using BingoVega.Models;
using BingoVega.Security;
using BingoVega.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BingoVega.FrontEnd.Utils
{
    public static class Api
    {
        private const string _tripleDesKey = "58d8b19df78558d2d0e5f509";

        public static void callCxd(HttpRequest Request, string userId)
        {
            string cxd, affiliate, ip = "";

            cxd = Cookie.getValue(Request, "cxd");

            if (!string.IsNullOrEmpty(cxd))
            {
                affiliate = Cookie.getValue(Request, "affiliateid");
                ip = Cookie.getValue(Request, "ipCliente");

                var url = $"https://cx-api.bngvg.com/player/signup?userId={userId}&affiliateId={affiliate}&cxd={cxd}&ip={ip}";
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "c7abf8f7be4aea4be10b7004587e9bee");
                //client.DefaultRequestHeaders.Add("Bearer", "c7abf8f7be4aea4be10b7004587e9bee");
                var response = client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonString = response.Content.ReadAsStringAsync().Result;
                }
            }
        }

        public static string AddUser(HttpRequest Request, IConfiguration config, UserProfile newUser)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;

            try { 
            HttpClient client = new HttpClient();
            StringContent content = new StringContent(JsonConvert.SerializeObject(newUser), Encoding.UTF8, "application/json");
            var response = client.PostAsync(apiUrl + "/user/addUser", content).Result;
            string jsonString = "";
                if (response.IsSuccessStatusCode)
                {
                    jsonString = response.Content.ReadAsStringAsync().Result;
                }
                return jsonString;
            }
            catch(Exception ex)
            {
                return "";
            }
        }

        public static string UpdateUser( IConfiguration config, UserProfile newUser)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;

            try
            {
                HttpClient client = new HttpClient();
                StringContent content = new StringContent(JsonConvert.SerializeObject(newUser), Encoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl + "/user/updateUser", content).Result;
                string jsonString = "";
                if (response.IsSuccessStatusCode)
                {
                    jsonString = response.Content.ReadAsStringAsync().Result;
                }
                return jsonString;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string UpdatePassword(IConfiguration config, string userId, string newPassword)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            try
            {
                HttpClient client = new HttpClient();                

                string param = $"userId={userId}&newPassword={newPassword}";
                HttpContent content = new StringContent(param, Encoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl + "/user/updateUserPassword?" + param, content).Result;

                string jsonString = "";
                if (response.IsSuccessStatusCode)
                {
                    jsonString = response.Content.ReadAsStringAsync().Result;
                }
                return jsonString;

            }
            catch ( Exception ex)
            {
                return "";
            }

        }

        public static bool AuthenticateUser(IConfiguration config, string username, string password)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            HttpClient client = new HttpClient();
            string url = string.Format("{2}/user/authenticate?username={0}&password={1}", username, password, apiUrl);
            var c = new StringContent(" ", Encoding.UTF8, "application/json");
            try
            {
                var response = client.GetAsync(url).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool GetLobbyAccessLevel(IConfiguration config, HttpContext h)
        {
            string userId = Session.getCurrentUserId(h);

            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            HttpClient client = new HttpClient();
            string url = string.Format("{1}/user/getLobbyAccessLevel?userId={0}", userId, apiUrl);            
            try
            {
                var response = client.GetAsync(url).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string a= response.Content.ReadAsStringAsync().Result;
                    return !bool.Parse(a);
                    
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static UserProfileViewModel GetUserProfile(IConfiguration config, string username)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            HttpClient client = new HttpClient();
            string url = string.Format("{1}/user/GetUser?username={0}", username, apiUrl);
            
            try
            {
                var response = client.GetAsync(url).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    UserProfileViewModel user = JsonConvert.DeserializeObject<UserProfileViewModel>(responseString);
                    return user;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<BingoRoom> GetBingoRooms()
        {            
            HttpClient client = new HttpClient();
            string url = "http://172.22.2.87:3001/api/bingo/fetch/lobbyrooms";

            try
            {   
                var response = client.GetAsync(url).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {                                             
                    XmlSerializer ser = new XmlSerializer(typeof(BingoRoom));                        
                    string responseString = response.Content.ReadAsStringAsync().Result;                  

                    TextReader tr = new StringReader(responseString); 
                    XDocument doc = XDocument.Load(tr);
                    List<BingoRoom> rooms = new List<BingoRoom>();
                    BingoRoom room;
                    foreach (XElement element in doc.Descendants("root").Descendants("data"))
                    {
                        room = new BingoRoom();
                        room.CurrentTime = (element.Element("currentTime") == null) ? DateTime.MinValue : DateTime.Parse(element.Element("currentTime").Value);
                        room.RoomId = short.Parse(element.Element("roomId").Value);
                        room.RoomName = (element.Element("roomName") == null) ? "" : element.Element("roomName").Value;
                        room.Caller = (element.Element("caller") == null) ? "" : element.Element("caller").Value;
                        room.Skin = (element.Element("skin") == null) ? "" : element.Element("skin").Value;
                        room.Announcer = (element.Element("announcer") == null) ? "" : element.Element("announcer").Value;
                        room.ScheduleName = (element.Element("scheduleName") == null) ? "" : element.Element("scheduleName").Value;
                        room.ChatRoomName = (element.Element("chatRoomName") ==null) ? "" : element.Element("chatRoomName").Value;

                        foreach (XElement e in element.Descendants("gameInfo"))
                        {
                            room.GameInfo = new GameInfo();
                            room.GameInfo.GameName = (e.Element("gameName").Value==null) ? "" : e.Element("gameName").Value;
                            room.GameInfo.JackpotAmount = (e.Element("jackpotAmount").Value == null) ? decimal.MinValue : decimal.Parse(e.Element("jackpotAmount").Value);
                            room.GameInfo.Cost = (e.Element("cost").Value == null) ? decimal.MinValue : decimal.Parse(e.Element("cost").Value);
                            room.GameInfo.StartTime = (e.Element("startTime").Value == null) ? "" : e.Element("startTime").Value;
                            room.GameInfo.Prize = (e.Element("prize").Value == null) ? decimal.MinValue : decimal.Parse(e.Element("prize").Value);
                            room.GameInfo.Date = (e.Element("date").Value == null) ? DateTime.MinValue :  DateTime.Parse(e.Element("date").Value);
                        }


                        rooms.Add(room);
                    }
                     return rooms;
                }
                else
                return null;
               
            }
             catch (Exception ex)
            {
                return null;
            }
        }
                
        public static string GetTextforBingoRoomMarquee()
        {
            HttpClient client = new HttpClient();
            string url = "http://172.22.2.87:3001/api/bingo/lobbymessage";
            string texto = "";
            try
            {
                var response = client.GetAsync(url).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    dynamic responceViewModel = JsonConvert.DeserializeObject<dynamic>(responseString);
                    if(responceViewModel.data!=null)
                    {
                        if (responceViewModel.data.data != null)
                        {
                            texto = responceViewModel.data.data;
                        }
                    }                                       
                    return texto;

                }
                else
                    return "An error has occurred with the end point, please reload the page!";
            }
            catch (Exception ex)
            {
                return "An error has occurred, please try again!";
            }
        }

        public static (string token, string jsessionid) GetUserCredentialsForBingoRooms(string userId)
        {
            HttpClient client = new HttpClient();
            string url = string.Format("http://172.22.2.87:3001/api/bingo/checkLogin?user={0}&xml=true", userId);
            string token = "";
            string session = "";
            try
            {
                var response = client.GetAsync(url).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    //XmlSerializer ser = new XmlSerializer(string);
                    string responseString = response.Content.ReadAsStringAsync().Result;

                    TextReader tr = new StringReader(responseString);
                    XDocument doc = XDocument.Load(tr);
                    List<BingoRoom> rooms = new List<BingoRoom>();
                    BingoRoom room;
                    foreach (XElement element in doc.Descendants("root").Descendants("data"))
                    {
                        token = element.Element("token").Value;
                        session = element.Element("jsessionid").Value;
                    }

                    return (token, session);
                }
                else
                    return ("", "");

            }
            catch (Exception ex)
            {
                return ("", ""); 
            }
        }

        public static List<string> GetCountries(IConfiguration config)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            HttpClient client = new HttpClient();
            string url = string.Format("{0}/user/getCountries", apiUrl);

            try
            {
                var response = client.GetAsync(url).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    List<string> countries = JsonConvert.DeserializeObject<List<string>>(responseString);
                    return countries;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<CreditCard> GetCreditCards(string userId, IConfiguration config)
        {
            List<CreditCard> cards = new List<CreditCard>();
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            HttpClient client = new HttpClient();
            string url = string.Format("{0}/Bank/getCards?userId={1}", apiUrl, userId);

            try
            {
                var response = client.GetAsync(url).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    List<CreditCardEncripted> cardsencrypted = JsonConvert.DeserializeObject<List<CreditCardEncripted>>(responseString);
                    foreach (var c in cardsencrypted) {
                        CreditCard nc = new CreditCard();
                        nc.Id = c.Id;
                        nc.CardType = c.CardType;
                        nc.CardNumber = Decryptor.Decrypt(c.CardNumber, _tripleDesKey);
                        nc.ExpiryYear = int.Parse(Decryptor.Decrypt(c.ExpiryYear, _tripleDesKey));
                        nc.ExpiryMonth = int.Parse(Decryptor.Decrypt(c.ExpiryMonth, _tripleDesKey));
                        nc.FirstName = c.FirstName;
                        nc.LastName = c.LastName;
                        nc.MiddleName = c.MiddleName;
                        nc.Address = Decryptor.Decrypt(c.Address, _tripleDesKey);
                        nc.Country = Decryptor.Decrypt(c.Country, _tripleDesKey);
                        nc.Postalcode = Decryptor.Decrypt(c.Postalcode, _tripleDesKey);
                        nc.Province = Decryptor.Decrypt(c.Province, _tripleDesKey);
                        nc.City = Decryptor.Decrypt(c.City, _tripleDesKey);
                        nc.Phone = Decryptor.Decrypt(c.Phone, _tripleDesKey);
                        cards.Add(nc);
                    }

                    return cards;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<PaymentMethod> GetPaymentMethods(int typeOfTransaction, IConfiguration config)
        {            
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            HttpClient client = new HttpClient();
            string url = string.Format("{0}/Bank/getPaymentMethods?TypeOfTransaction={1}", apiUrl, typeOfTransaction);

            try
            {
                var response = client.GetAsync(url).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    var methods = JsonConvert.DeserializeObject<List<PaymentMethod>>(responseString);                   

                    return methods;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static CreditCard GetCreditCard(int idCard, string userId, IConfiguration config)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            HttpClient client = new HttpClient();
            string url = string.Format("{0}/Bank/getCard?Id={1}&userId={2}", apiUrl, idCard, userId);

            try
            {
                var response = client.GetAsync(url).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    var cardEncrypted = JsonConvert.DeserializeObject<CreditCardEncripted>(responseString);

                    CreditCard cardDesEncrypted = new CreditCard();
                    cardDesEncrypted.Id = cardEncrypted.Id;
                    cardDesEncrypted.CardTypeDescription = cardEncrypted.CardTypeDescription;
                    cardDesEncrypted.CardType = cardEncrypted.CardType;
                    cardDesEncrypted.CardNumber = Decryptor.Decrypt(cardEncrypted.CardNumber, _tripleDesKey);
                    cardDesEncrypted.ExpiryYear = int.Parse(Decryptor.Decrypt(cardEncrypted.ExpiryYear, _tripleDesKey));
                    cardDesEncrypted.ExpiryMonth = int.Parse(Decryptor.Decrypt(cardEncrypted.ExpiryMonth, _tripleDesKey));
                    cardDesEncrypted.FirstName = cardEncrypted.FirstName;
                    cardDesEncrypted.LastName = cardEncrypted.LastName;
                    cardDesEncrypted.MiddleName = cardEncrypted.MiddleName;
                    cardDesEncrypted.Address = Decryptor.Decrypt(cardEncrypted.Address, _tripleDesKey);
                    cardDesEncrypted.Country = Decryptor.Decrypt(cardEncrypted.Country, _tripleDesKey);
                    cardDesEncrypted.Postalcode = Decryptor.Decrypt(cardEncrypted.Postalcode, _tripleDesKey);
                    cardDesEncrypted.Province = Decryptor.Decrypt(cardEncrypted.Province, _tripleDesKey);
                    cardDesEncrypted.City = Decryptor.Decrypt(cardEncrypted.City, _tripleDesKey);
                    cardDesEncrypted.Phone = Decryptor.Decrypt(cardEncrypted.Phone, _tripleDesKey);
                    
                    return cardDesEncrypted;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static int GetTransactionNumber(IConfiguration config)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            HttpClient client = new HttpClient();
            string url = string.Format("{0}/bank/getTransactionNumber", apiUrl);
            try
            {
                var response = client.GetAsync(url).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string a = response.Content.ReadAsStringAsync().Result;
                    return int.Parse(a);

                }
                else
                    throw new Exception();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void AddCreditCard(IConfiguration config, CreditCard card)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;

            try
            {
                HttpClient client = new HttpClient();
                StringContent content = new StringContent(JsonConvert.SerializeObject(card), Encoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl + "/Bank/addCreditCard", content).Result;                
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception();
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void EditCreditCard(IConfiguration config, CreditCard card)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;

            try
            {
                HttpClient client = new HttpClient();
                StringContent content = new StringContent(JsonConvert.SerializeObject(card), Encoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl + "/Bank/editCreditCard", content).Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void DeleteCreditCard(CreditCard card, IConfiguration config)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;

            try
            {
                HttpClient client = new HttpClient();
                StringContent content = new StringContent(JsonConvert.SerializeObject(card), Encoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl + "/Bank/deleteCreditCard", content).Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static int BeginDepositTransaction(string userId, string amount, string cctype, int cc_id, string paymenttypeid, IConfiguration config)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;

            try
            {
                HttpClient client = new HttpClient();               
                string param = $"userId={userId}&amount={amount}&cctype={cctype}&cc_id={cc_id}&paymenttypeid={paymenttypeid}";
                HttpContent content = new StringContent(param, Encoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl + "/Bank/beginTransaction?" + param, content).Result;

                string jsonString = "";
                if (response.IsSuccessStatusCode)
                {
                    jsonString = response.Content.ReadAsStringAsync().Result;
                }
                return int.Parse(jsonString);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static bool CreateWithdrawalRequest(IConfiguration config, string accountId, double amount, int paymentMethod)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            bool result = false;

            try
            {
                HttpClient client = new HttpClient();

                var data = new
                {
                    accountId = accountId,
                    amount = amount,
                    paymentMethod= paymentMethod
                };
                                
                var response = client.PostAsJsonAsync(apiUrl + "/Bank/createWithdrawalRequest", data).Result;
                if (response.IsSuccessStatusCode)
                {                    
                    result = bool.Parse(response.Content.ReadAsStringAsync().Result);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static Withdrawal GetPendingWithdrawal(string userId, IConfiguration config)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            HttpClient client = new HttpClient();
            string url = string.Format("{0}/Bank/getPendingWithdrawal?userId={1}", apiUrl, userId);

            try
            {
                var response = client.GetAsync(url).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    var withdrawal = JsonConvert.DeserializeObject<Withdrawal>(responseString);

                    return withdrawal;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool CancelWithdrawalRequest(IConfiguration config, int transaction, string accountId, double amount, int paymentMethod)
        {
            string apiUrl = config.GetSection("ApiConfig").GetSection("url").Value;
            bool result = false;

            try
            {
                HttpClient client = new HttpClient();

                var data = new
                {
                    transaction = transaction,
                    accountId = accountId,
                    amount = amount,
                    paymentMethodId = paymentMethod
                };

                var response = client.PostAsJsonAsync(apiUrl + "/Bank/cancelWithdrawalRequest", data).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = bool.Parse(response.Content.ReadAsStringAsync().Result);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
