﻿using Microsoft.AspNetCore.Http;

namespace BingoVega.FrontEnd.Utils
{
    public static class Session
    {
        public static string  getCurrentUser(HttpContext h)
        {
            return h.Session.GetString("username");
        }

        public static string getCurrentUserId(HttpContext h)
        {
            return h.Session.GetString("userId");
        }

        public static string getUserEmail(HttpContext h)
        {
            return h.Session.GetString("email");
        }

        public static string getUserBalance(HttpContext h)
        {
            return h.Session.GetString("balance");
        }

        public static string getUserCash(HttpContext h)
        {
            return h.Session.GetString("cash");
        }

        public static string getUserBonus(HttpContext h)
        {
            return h.Session.GetString("bonus");
        }

        public static int? getUserDeposits(HttpContext h)
        {
            return h.Session.GetInt32("deposits");
        }

        public static string getUserPostalCode(HttpContext h)
        {
            return h.Session.GetString("postalcode");
        }

        public static bool getUserHasWithdrawal(HttpContext h)
        {
            string a = h.Session.GetString("hasWithdrawal");
            return bool.Parse(a);
        }

        public static double getAmountToDeposit(HttpContext h)
        {
            string a = h.Session.GetString("amounttodeposit");
            return double.Parse(a);
        }

        public static double getMaxBalanceToDeposit(HttpContext h)
        {
            string a = h.Session.GetString("maxbalancetodeposit");
            return double.Parse(a);
        }

        public static void saveMaxBalanceToDeposit(HttpContext h, string balance)
        {
            h.Session.SetString("maxbalancetodeposit", balance);
        }

        public static void saveAmountToDeposit(HttpContext h, string amount)
        {
            h.Session.SetString ("amounttodeposit", amount);
        }

        public static void saveCurrentUser(HttpContext h, string username)
        {
            h.Session.SetString("username", username);
        }

        public static void saveCurrentUserId(HttpContext h, string userId)
        {
            h.Session.SetString("userId", userId);
        }

        public static void saveNameCurrentUser(HttpContext h, string name)
        {
            h.Session.SetString("firstName", name);
        }

        public static void saveIsUserLogedIn(HttpContext h, bool state)
        {            
            h.Session.SetString("isLogedIn", state.ToString());
        }

        public static void saveUserBalance(HttpContext h, string balance)
        {
            h.Session.SetString("balance", balance);
        }

        public static void saveUserCash(HttpContext h, string cash)
        {
            h.Session.SetString("cash", cash);
        }

        public static void saveUserBonus(HttpContext h, string bonus)
        {
            h.Session.SetString("bonus", bonus);
        }

        public static void saveUserDeposits(HttpContext h, int deposits)
        {
            h.Session.SetInt32("deposits", deposits);
        }

        public static void saveUserEmail(HttpContext h, string email)
        {
            h.Session.SetString("email", email);
        }
                                   
        public static void saveUserPostalCode(HttpContext h, string postalcode)
        {
            h.Session.SetString("postalcode", postalcode);
        }

        public static void saveUserHasWithdrawal(HttpContext h, bool hasWithdrawal)
        {
            h.Session.SetString("hasWithdrawal", hasWithdrawal.ToString());
        }

        public static bool isUserLogedIn(HttpContext h)
        {            
            bool r= (h.Session.GetString("isLogedIn") != null && h.Session.GetString("isLogedIn") == bool.TrueString) ? true : false;
            return r;
        }

        public static void closeSession(HttpContext h)
        {
            h.Session.Clear();  
        }



    }
}
