﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BingoVega.FrontEnd.Models
{
    public class Withdrawal
    {

        public DateTime TransactionDate { get; set; }
        public int TransactionNumber { get; set; }
        public double Amount { get; set; }
        public double Balance { get; set; }
        public int PaymentMethodId { get; set; }
        public string cctranid { get; set; }


    }
}
