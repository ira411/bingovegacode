﻿using BingoVega.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Models
{
    public class CreditCardViewModel
    {
        public CreditCard card { get; set; }
        public string ExpiredDate { get; set; }
        public List<CreditCardType> CrediCardTypes { get; set; }        
        public List<string> Countries { get; set; }

    }
}
