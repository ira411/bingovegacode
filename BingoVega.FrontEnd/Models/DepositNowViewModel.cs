﻿using BingoVega.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Models
{
    public class DepositNowViewModel
    {
        public string creditId;
        public double Amount { get; set; }
        public List<decimal>Amounts { get; set; }
        public string PromoCode { get; set; }
        public List<CreditCard> CreditCards { get; set; }        

        public double Total { get; set; }
        public double Bonus { get; set; }
        public double Available { get; set; }

    }
}
