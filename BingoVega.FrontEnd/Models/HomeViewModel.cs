﻿using BingoVega.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Models
{
    public class HomeViewModel
    {
        public List<BigoRoom> BingoRooms { get; set; }
    }
}
