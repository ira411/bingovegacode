﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BingoVega.FrontEnd.Models
{
    public class GameInfo
    {
        public string GameName { get; set; }
        public decimal JackpotAmount { get; set; }
        public decimal Cost { get; set; } 
        public string StartTime { get; set; }
        public decimal Prize { get; set; }    
        public DateTime Date { get; set; }

    }
}
