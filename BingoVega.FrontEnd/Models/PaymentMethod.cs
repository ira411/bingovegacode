﻿namespace BingoVega.FrontEnd.Models
{
    public struct PaymentMethod
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
