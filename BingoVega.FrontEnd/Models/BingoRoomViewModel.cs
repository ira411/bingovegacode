﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Models
{
    public class BingoRoomViewModel
    {
        public List<BingoRoom> Rooms { get; set; }

        public bool HasFullLobbyAccess { get; set; }
        public string TextForMarquee { get; set; }
        public string UrlForPreOrderCards { get; set; }
        public string UserId { get; set; }
        public string Token { get; set; }
        public string JsessionId { get; set; }

    }
}
