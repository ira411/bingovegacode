﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.ViewModels
{
    public class UserProfileViewModel
    {
        public UserProfileViewModel()
        {
            Provinces = new List<string>();
            Provinces.Add("");        
        }

        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Alias is required")]        
        public string Username { get; set; } 

        public string ClassName { get; set; }
        public string Country { get; set; }

        [Required(ErrorMessage = "Postal code is required")]
        public string PostalCode { get; set; }

        public string Province { get; set; }
        public string ProvinceDropDown { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Required(ErrorMessage = "Address is required")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Phone is required")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email format is not correct")]
        public string Email { get; set; }

        [Compare("Email", ErrorMessage = "Email doesn't match")]
        public string ConfirmEmail { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Password doesn't match")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
                
        public bool AceptTerms { get; set; }

        public string UserId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Language { get; set; }
        public string Active { get; set; }
        public string Activated { get; set; }
        public string Question { get; set; }
        public string Hint { get; set; }
        public string Answer { get; set; }        
        public string BirthDay { get; set; }
        public int SourceId { get; set; }        
        public string SourceName
        {
            get
            {
                switch (SourceId)
                {
                    case 1:
                        return "Other Player";
                    case 2:
                        return "Search Engine";
                    case 3:
                        return "TV Ad";
                    case 4:
                        return "Affiliate Site";
                    default:
                        return "";
                }
            }
            set { }
        }
        public string Gender { get; set; }
        public string ReceiveBroadCast { get; set; }

        public decimal? Balance { get; set; }
        public decimal? Winnings { get; set; }
        public decimal? Bank { get; set; }
        public decimal? Cash { get; set; }
        public decimal? Bonus { get; set; }

        public string Message { get; set; }
        public List<string> Countries { get; set; }
        public List<string> Provinces { get; set; }
        public int CreditCardsAllowed { get; set; }
        public int DepositsLastMonth { get; set; }
        public double MaxBalanceToDeposit { get; set; }

    }
}
