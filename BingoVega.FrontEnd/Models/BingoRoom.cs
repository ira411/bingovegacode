﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BingoVega.FrontEnd.Models
{
    public class BingoRoom
    {

        public DateTime? CurrentTime { get; set; }
        public int RoomId { get; set; }
        public string RoomName { get; set; }
        public string Caller { get; set; }
        public string Skin { get; set; }
        public string Announcer { get; set; }
        public string ScheduleName { get; set; }
        public string ChatRoomName { get; set; }
        public GameInfo GameInfo { get; set; }

    }
}
