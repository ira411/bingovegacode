﻿using BingoVega.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BingoVega.FrontEnd.Models
{
    public class BankViewModel
    {       
        public decimal Amount { get; set; }
        public decimal WithdrawalAmount { get; set; }
        public decimal EcheckAmount { get; set; }
        public string creditId;
        public List<decimal>Amounts { get; set; }
        public string PromoCode { get; set; }
        public List<CreditCard> CreditCards { get; set; }

        public double Total { get; set; }
        public double Bonus { get; set; }
        public double Available { get; set; }
        public int? DepositsLastMonth { get; set; }
        public List<PaymentMethod> PaymentMethods { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public bool HasWithdrawal { get; set; }
        public Withdrawal Withdrawal { get; set; }

        public bool BalanceExceedsMax { get; set; }

    }
}
