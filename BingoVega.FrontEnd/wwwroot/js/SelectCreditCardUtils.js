
function isEmpty(value) {
    if (value.trim().length == 0) {       
        return true;
    } else {        
        return false;
    }
};

function isAValidCvv(value) {
    if (value.trim().length ==3 || value.trim().length == 4) {
        return true;
    } else {
        return false;
    }
};

function isAValidPwd(value) {
    if (value.trim().length >= 8){
        return true;
    } else {
        return false;
    }
};