function setDefaultValuesToControls() {
    $("#vmUsername").css('display', 'none');
    $("#vmUsernameFormat").css('display', 'none');
    $("#vmUsernameLength").css('display', 'none');
    $("#vmAceptTerms").css('display', 'none');
    $("#vmPhone").css('display', 'none');
    $("#vmCity").css('display', 'none');
    $("#vmPostalCode").css('display', 'none');
    $("#vmAddress").css('display', 'none');
    $("#vmLastName").css('display', 'none');
    $("#vmFirstName").css('display', 'none');
    $("#vmConfirmEmail").css('display', 'none');
    $("#vmEmail").css('display', 'none');
    $("#vmEmailFormat").css('display', 'none');
    $("#vmConfirmPassword").css('display', 'none');
    $("#vmPassword").css('display', 'none');
    $("#vmPasswordLength").css('display', 'none');
    $("#ProvinceDropDown").css('display', 'none');
    $("#vmGeneralMessage").css('display', 'none');
};

function getStatesOfCountry(country) {

    if (country == "CANADA" ) {
        return getStatesOfCanada();
    }
    else if (country == "UNITED KINGDOM" ) {
        return getStatesOfUnitedKingdom();
    } else if ( country == "UNITED STATES") {
        return getStatesOfUSA();
    } 
};

function getStatesOfCanada() {
    var list = [
        { Code: 'AB', Name: 'Alberta' }, 
        { Code: 'BC', Name: 'British Columbia' },
        { Code: 'MB', Name: 'Manitoba' },     
        { Code: 'NL', Name: 'Newfoundland and Labrador' },          
        { Code: 'NB', Name: 'New Brunswick' }, 
        { Code: 'NT', Name: 'Northwest Territories' },
        { Code: 'NS', Name: 'Nova Scotia' },
        { Code: 'NU', Name: 'Nunavut' }, 
        { Code: 'ON', Name: 'Ontario' },  
        { Code: 'PE', Name: 'Prince Edward Island' },        
        { Code: 'QC', Name: 'Quebec' },        
        { Code: 'SK', Name: 'Saskatchewan' },  
        { Code: 'YT', Name: 'Yukon Territory'}, 
    ];

    return list;
};

function getStatesOfUSA() {
    var list = [
        { Code: 'AL', Name: 'Alabama' },
        { Code: 'AK', Name: 'Alaska' },
        { Code: 'AZ', Name: 'Arizona' },
        { Code: 'AR', Name: 'Arkansas' },
        { Code: 'BC', Name: 'British Columbia' },        
        { Code: 'CA', Name: 'California' },
        { Code: 'CO', Name: 'Colorado' },
        { Code: 'CT', Name: 'Connecticut' },
        { Code: 'DE', Name: 'Delaware' },
        { Code: 'DC', Name: 'District Of Columbia' },
        { Code: 'FL', Name: 'Florida' },
        { Code: 'GA', Name: 'Georgia' },
        { Code: 'HI', Name: 'Hawaii' },
        { Code: 'ID', Name: 'Idaho' },
        { Code: 'IL', Name: 'Illinois' },
        { Code: 'IN', Name: 'Indiana' },
        { Code: 'IA', Name: 'Iowa' },
        { Code: 'KS', Name: 'Kansas' },
        { Code: 'KY', Name: 'Kentucky' },
        { Code: 'LA', Name: 'Louisiana' },
        { Code: 'ME', Name: 'Maine' },
        { Code: 'MD', Name: 'Maryland' },
        { Code: 'MA', Name: 'Massachusetts' },
        { Code: 'MI', Name: 'Michigan' },
        { Code: 'MN', Name: 'Minnesota' },
        { Code: 'MS', Name: 'Mississippi' },
        { Code: 'MO', Name: 'Missouri' },
        { Code: 'MT', Name: 'Montana' },
        { Code: 'NE', Name: 'Nebraska' },
        { Code: 'NV', Name: 'Nevada' },
        { Code: 'NH', Name: 'New Hampshire' },
        { Code: 'NJ', Name: 'New Jersey' },
        { Code: 'NM', Name: 'New Mexico' },
        { Code: 'NY', Name: 'New York' },
        { Code: 'NC', Name: 'North Carolina' },
        { Code: 'ND', Name: 'North Dakota' },
        { Code: 'OH', Name: 'Ohio' },
        { Code: 'OK', Name: 'Oklahoma' },
        { Code: 'OR', Name: 'Oregon' },
        { Code: 'PA', Name: 'Pennsylvania' },
        { Code: 'RI', Name: 'Rhode Island' },
        { Code: 'SC', Name: 'South Carolina' },
        { Code: 'SD', Name: 'South Dakota' },
        { Code: 'TN', Name: 'Tennessee' },
        { Code: 'TX', Name: 'Texas' },
        { Code: 'UT', Name: 'Utah' },
        { Code: 'VT', Name: 'Vermont' },
        { Code: 'VA', Name: 'Virginia' },
        { Code: 'WA', Name: 'Washington' },
        { Code: 'WV', Name: 'West Virginia' },
        { Code: 'WI', Name: 'Wisconsin' },
        { Code: 'WY', Name: 'Wyoming' },
    ];

    return list;
}
       
function getStatesOfUnitedKingdom() {
    var list = [
        { Name: 'Angus', Code: 'ANS' },
        { Name: 'Argyll', Code: 'ARL' },
        { Name: 'Ayrshire', Code: 'AYR' },
        { Name: 'Aberdeenshire', Code: 'ABD' },
        { Code: 'AVN', Name: 'Avon' },
        { Name: 'Antrim', Code: 'ANT' },
        { Name: 'Armagh', Code: 'ARM' },
        { Code: 'BC', Name: 'British Columbia' },
        { Code: 'BDF', Name: 'Bedfordshire'},
        { Code: 'BRK', Name: 'Berkshire'},
        { Code: 'BKM', Name: 'Buckinghamshire' },
        { Name: 'Banffshire', Code: 'BAN' },
        { Name: 'Berwickshire', Code: 'BEW' },
        { Name: 'Bute', Code: 'BUT' },
        { Code: 'CAM', Name: 'Cambridgeshire'},
        { Code: 'CHS', Name: 'Cheshire'},
        { Code: 'CLV', Name: 'Cleveland'},
        { Code: 'CON', Name: 'Cornwall'},
        { Code: 'CMA', Name: 'Cumbria' },
        { Name: 'Caithness', Code: 'CAI' },
        { Name: 'Clackmannanshire', Code: 'CLK' },
        { Name: 'Clwyd', Code: 'CWD' },
        { Code: 'DBY', Name: 'Derbyshire'},
        { Code: 'DEV', Name: 'Devon'},
        { Code: 'DOR', Name: 'Dorset'},
        { Code: 'DUR', Name: 'Durham' },
        { Name: 'Dumfriesshire', Code: 'DGY' },
        { Name: 'Dunbartonshire', Code: 'DNB' },
        { Name: 'Dyfed', Code: 'DFD' },       
        { Name: 'Down', Code: 'DOW' },
        { Code: 'SXE', Name: 'East Sussex'},
        { Code: 'ESS', Name: 'Essex' },
        { Name: 'East Lothian', Code: 'ELN' },
        { Name: 'Fife', Code: 'FIF' },
        { Name: 'Fermanagh', Code: 'FER' },
        { Code: 'GLS', Name: 'Gloucestershire' },
        { Name: 'Gwent', Code: 'GNT' },
        { Name: 'Gwynedd', Code: 'GWN' },
        { Code: 'HAM', Name: 'Hampshire'},
        { Code: 'HEF', Name: 'Herefordshire'},
        { Code: 'HRT', Name: 'Hertfordshire'},
        { Code: 'IOW', Name: 'Isle of Wight' },
        { Name: 'Inverness-shire', Code: 'INV' },
        { Code: 'KEN', Name: 'Kent'},
        { Name: 'Kincardineshire', Code: 'KCD', },
        { Name: 'Kinross-shire', Code: 'KRS' },
        { Name: 'Kirkcudbrightshire', Code: 'KKD' },
        { Name: 'Lanarkshire', Code: 'LKS' },
        { Name: 'Londonderry', Code: 'LDY' },
        { Code: 'LAN', Name: 'Lancashire' },
        { Code: 'LEI', Name: 'Leicestershire' },
        { Code: 'LIN', Name: 'Lincolnshire' },
        { Name: 'London', Code: 'LDN' },
        { Name: 'Midlothian', Code: 'MLN' },
        { Name: 'Moray', Code: 'MOR' },
        { Name: 'Nairnshire', Code: 'NAI' },
        { Name: 'Mid Glamorgan', Code: 'MGM' },
        { Name: 'Merseyside', Code: 'MSY'},
        { Name: 'Norfolk',Code: 'NFK'},
        { Name: 'Northamptonshire', Code: 'NTH'},
        { Name: 'Northumberland', Code: 'NBL'},
        { Name: 'North Yorkshire', Code: 'NYK'},
        { Name: 'Nottinghamshire', Code: 'NTT'},
        { Name: 'Oxfordshire', Code: 'OXF' },
        { Name: 'Orkney', Code: 'OKI' },
        { Code: 'ON', Name: 'Ontario' },
        { Name: 'Powys', Code: 'POW' },
        { Name: 'Peeblesshire', Code: 'PEE' },
        { Name: 'Perthshire', Code: 'PER' },
        { Name: 'Renfrewshire', Code: 'RFW' },
        { Name: 'Ross-shire', Code: 'ROC' },
        { Name: 'Roxburghshire', Code: 'ROX' },
        { Name: 'Rutland', Code: 'RUT'},
        { Name: 'Shropshire',  Code: 'SAL'},
        { Name: 'Somerset', Code: 'SOM'},
        { Name: 'South Yorkshire',Code: 'SYK'},
        { Name: 'Staffordshire', Code: 'STS'},
        { Name: 'Suffolk', Code: 'SFK'},
        { Name: 'Surrey', Code: 'SRY' },
        { Name: 'South Glamorgan', Code: 'SGM' },
        { Name: 'Selkirkshire', Code: 'SEL' },
        { Name: 'Shetland', Code: 'SHI' },
        { Name: 'Stirlingshire', Code: 'STI' },
        { Name: 'Sutherland', Code: 'SUT' },
        { Name: 'Tyne and Wear', Code: 'TWR' },
        { Name: 'Tyrone', Code: 'TYR' },      
        { Name: 'Warwickshire',Code: 'WAR'},
        {Name: 'West Midlands',Code: 'WMD'},
        {Name: 'West Sussex', Code: 'SXW'},
        {Name: 'West Yorkshire', Code: 'WYK'},
        {Name: 'Wiltshire',Code: 'WIL'},
        { Name: 'Worcestershire', Code: 'WOR' },
        { Name: 'West Glamorgan', Code: 'WGM' },
        { Name: 'West Lothian', Code: 'WLN' },
        { Name: 'Wigtownshire', Code: 'WIG' },       
      
    ];

    return list;
};

function hideProvinces(hide) {
    if(hide){
        $("#ProvinceDropDown").css('display', 'none');
        $("#Province").css('display', 'inline');
    }else {
        $("#ProvinceDropDown").css('display', 'inline');
        $("#Province").css('display', 'none');

    }
}

function isAliasLengthValid() {   
    var n = $("#Username").val().length
    if (n < 6) {
        $("#vmUsernameLength").css('display', 'inline');
        return false;
    } else {
        $("#vmUsernameLength").css('display', 'none');
        return true;
    }   
}

function isAliasFormatValid() {  
    var regex = new RegExp("^[a-zA-Z0-9-]+$");     
    if (regex.test($("#Username").val())) {
        $("#vmUsernameFormat").css('display', 'none');
        return true;
    } else {
        $("#vmUsernameFormat").css('display', 'inline');
        return false;
    }
}

function isEmailFormatValid() {
    let res = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (res.test($("#Email").val())) {
        $("#vmEmailFormat").css('display', 'none');
        return true;
    } else {
        $("#vmEmailFormat").css('display', 'inline');
        return false;
    }
}

function isPasswordLenghtValid() {
    var n = $("#Password").val().length
    if (n < 8) {
        $("#vmPasswordLength").css('display', 'inline');
        return false;
    } else {
        $("#vmPasswordLength").css('display', 'none');
        return true;
    }   
};

function userAcceptTerms() {
    if ($("#AceptTerms").prop("checked") == false) {
        $("#vmAceptTerms").css('display', 'inline');
        return false;
    } else {
        $("#vmAceptTerms").css('display', 'none');
        return true;
    }
};

function areConfirmationControlValid() {
    var cont = 0;
   
    if ($("#ConfirmPassword").val() != $("#Password").val()) {
        $("#vmConfirmPassword").css('display', 'inline');
        cont++;
    } else {
        $("#vmConfirmPassword").css('display', 'none');
    }

    if ($("#Email").val() != $("#ConfirmEmail").val()) {
        $("#vmConfirmEmail").css('display', 'inline');
        cont++;
    } else {
        $("#vmConfirmEmail").css('display', 'none');
    }
    

    if (cont > 0) {
        return false;
    } else {
        return true;
    }
};

function informationIsMissing(value) {

    var controls = ["Username", "Phone", "City", "PostalCode", "Address", "LastName", "FirstName", "Email", "Password"];
    var messages = ["vmUsername", "vmPhone", "vmCity", "vmPostalCode", "vmAddress", "vmLastName", "vmFirstName", "vmEmail", "vmPassword"];
    var cont = 0;

    for (var i = 0; i < controls.length; i++) {
        if (isEmpty($("#" + controls[i]).val() + "", messages[i])) {
            cont++;
        }
    }

    if (cont > 0) {
        return true;
    } else {
        return false;
    }
};

function isEmpty(value, idValidationMessage) {
    var vm = $("#" + idValidationMessage + "");
    if (value.trim().length == 0) {
        vm.css('display', 'inline');
        return true;
    } else {
        vm.css('display', 'none');
        return false;
    }
};