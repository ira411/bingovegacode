﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

/**
* Back to top button
*/

var btn = $('#button');

$(window).scroll(function () {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
});

/**
* Dropdown Login Form
*/

$(document).on("click", ".navbar-right .dropdown-menu", function (e) {
    e.stopPropagation();
});

$('a').click(function (e) {
    $('.collapse').collapse('hide');
});

$(".submenuContent a").click(function () {
    // Instead of directly editing CSS, toggle a class
    $("a").removeClass("clicked");//removes classes everywhere
    $(this).toggleClass("clicked");//adds the class at the right button
});

/**
 * FLOATING SHOUPE
 */

$(function () {
    $.fn.scrollBottom = function () {
        return $(document).height() - this.scrollTop() - this.height();
    };

    var $el = $('.floatingShoupe');
    var $window = $(window);
    var top = $el.parent().position().top;

    $window.bind("scroll resize", function () {
        var gap = $window.height() - $el.height() - 10;
        var visibleFoot = 172 - $window.scrollBottom();
        var scrollTop = $window.scrollTop()

        if (scrollTop < top + 10) {
            $el.css({
                top: (top - scrollTop + 50) + "px",
                bottom: "auto"
            });
        } else if (visibleFoot > gap) {
            $el.css({
                top: "50px",
                bottom: visibleFoot + "px"
            });
        } else {
            $el.css({
                top: 50,
                bottom: "auto"
            });
        }
    }).scroll();
});

//Mobile Number Formatter
function formatPhoneNumber(value) {
    if (!value) return value;
    const phoneNumber = value.replace(/[^\d]/g, '');
    const phoneNumberLength = phoneNumber.length;
    if (phoneNumberLength < 4) return phoneNumber;
    if (phoneNumberLength < 7) {
        return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
    }
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3,6
    )}-${phoneNumber.slice(6, 9)}`;
}

function phoneNumberFormatter() {
    const inputField = document.getElementById('Phone');
    const formattedInputValue = formatPhoneNumber(inputField.value);
    inputField.value = formattedInputValue;
}

function CCphoneNumberFormatter() {
    const inputField = document.getElementById('card_Phone');
    const formattedInputValue = formatPhoneNumber(inputField.value);
    inputField.value = formattedInputValue;
}

//Credit Card Number Format
function formats(ele, e) {
    if (ele.value.length < 19) {
        ele.value = ele.value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
        return true;
    } else {
        return false;
    }
}

function numberValidation(e) {
    e.target.value = e.target.value.replace(/[^\d ]/g, '');
    return false;
}

//Credit Card Expiry Date format
function formatString(e) {
    var inputChar = String.fromCharCode(event.keyCode);
    var code = event.keyCode;
    var allowedKeys = [8];
    if (allowedKeys.indexOf(code) !== -1) {
        return;
    }

    event.target.value = event.target.value.replace(
        /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
    ).replace(
        /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
    ).replace(
        /^([0-1])([3-9])$/g, '0$1/$2' // 13 > 01/3
    ).replace(
        /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
    ).replace(
        /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
    ).replace(
        /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
    ).replace(
        /\/\//g, '/' // Prevent entering more than 1 `/`
    );
}

//SELECT USA COUNTRY
$(document).ready(function () {
    var textToFind = 'UNITED STATES';

    var dd = document.getElementById('Country');
    for (var i = 0; i < dd.options.length; i++) {
        if (dd.options[i].text === textToFind) {
            dd.selectedIndex = i;
            break;
        }
    }
    var country = $("#Country").val();

    if (country == "UNITED STATES") {

        var optionsValues = '<select class="form-select" id="ProvinceDropDown" style="text-transform=uppercase !important"><option value="">PLEASE SELECT YOUR STATE</option>';
        var states = getStatesOfCountry(country);
        states.forEach((e) => {
            optionsValues += '<option value="' + e.Code + '">' + e.Name + '</option>';
        });
        optionsValues += '</select>';
        var options = $('#ProvinceDropDown');
        options.replaceWith(optionsValues);
        hideProvinces(false);

    } else {
        $("#Province").val("");
    }
})


function on() {
    document.getElementById("overlay").style.display = "block";
}

function off() {
    document.getElementById("overlay").style.display = "none";
} 

